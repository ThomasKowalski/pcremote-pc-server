# README #

Le serveur PCRemote a été conçu pour fonctionner avec la plateforme Arduino, ainsi que le [programme dédié](https://bitbucket.org/ThomasKowalski/pcremote-arduino-sketch) pour ce dernier.
Si vous ne voulez que l'exécutable, [cliquez ici](https://bitbucket.org/ThomasKowalski/pcremote-pc-server/raw/master/PCRemote/bin/Debug/PCRemote.exe) !

### Comment ça marche ? ###

* Installez le sketch Arduino
* Soit vous voulez l'exécutable et vous suivez les étapes ci-dessus.
* Soit vous voulez la source et vous clonez le repository et compilez la solution.
* L'utilisation est détaillée à cette adresse : [thomaskowalski.net/pcremote](http://thomaskowalski.net/pcremote)
* Le développement est détaillé à cette adresse : [thomaskowalski.net/pcremote-programmation](http://thomaskowalski.net/pcremote-programmation)

### Logiciels inclus ###

*PCRemote utilise des logiciels externes pour faire certaines actions.*

* Bibliothèque de contrôle d'iTunes
* [WIZMO](https://www.grc.com/wizmo/wizmo.htm)
* [NirCMD](http://www.nirsoft.net/utils/nircmd.html)

### Besoin d'aide ? ###

N'hésitez pas à me contacter : [thomaskowalski.net/contact](http://thomaskowalski.net/contact)