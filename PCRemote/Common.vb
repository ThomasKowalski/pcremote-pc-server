﻿Imports System.IO.Ports

Module Common

    Public VersionPCRemote As String = "2.0"

    Public LastSecond As Integer = 0
    Public CompteurSeconde As Integer = 1

    Public MarqueurConnexion = "[Connexion COM]"
    Public MarqueurReception = "[Reception COM]"
    Public MarqueurInfos = "[Infos sur COM]"
    Public MarqueurConfiguration = "[Configuration]"
    Public MarqueurDependances = "[ Dépendances ]"
    Public MarqueurCore = "[PCRemote Core]"

    Public ListeActionsPossibles As New List(Of String) 'Déclaration de la liste des types d'actions possibles (iTunes, CMD...)
    Public ListeActionsPossiblesAvecParametres As New List(Of String) 'Déclaration de la liste des types d'actions possibles (iTunes, CMD...)

    Public WizmoInstance As Wizmo
    Public NirCMDInstance As NirCMD
    Public MainLogProvider As LogProvider
    Public MainParametersProvider As ParametersManager
    Public MainConfigurationParser As ConfigurationParser
    Public COMManager As COM

    Public iTunesObject As iTunesControler 'Initialisation du contrôleur iTunes
    Public LastAction As String
    Public LastTypeAction As String

    Public Sub LoadAllParameters() 'Appelle toutes les méthodes de chargement de paramètres
        ListeActionsPossibles = MainParametersProvider.ChargerActionsPossibles()
        ListeActionsPossiblesAvecParametres = MainParametersProvider.ChargerActionsPossiblesAvecParametres()
    End Sub

    Public Function Heure() As String 'Renvoie l'heure (utilisée pour le log)
        Return DateTime.Now.ToString("HH:mm:ss")
    End Function
    Public Function DateHeure() As String 'Renvoie la date et l'heure (utilisées pour les captures d'écran)
        Return DateTime.Now.ToString("yyyy-MM-dd") & " - " & DateTime.Now.ToString("HH-mm-ss")
    End Function


End Module


