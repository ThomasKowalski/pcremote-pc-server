﻿Module ModuleDump
    'Sub ShowString(ByVal myString As String)
    '    If TimerCoDeco.Enabled Then Exit Sub 'Oui ça en fait c'est un timer qui est là parce que quand j'envoie un message à l'Arduino ce qui a pour effet de foutre la merde un petit peu. Du coup j'attends 100ms (je crois) avant de prendre de nouveau en compte les données.
    '    If TimerReception.Enabled = True Then 'Pareil qu'au dessus, j'attends un certain temps entre chaque réception, pour éviter de faire trop d'actions à la fois à cause d'un bouton laissé appuyé un poil trop longtemps
    '        TempString = ""
    '        Exit Sub
    '    End If
    '    TempString &= myString 'Dans le cas où on accepte les données, on ajoute la dernière chaîne reçue à celle d'avant. Parce que tel un bad boy je reçois les signaux par morceaux.
    '    If myString.Contains(vbCrLf) Then
    '        myString = TempString.Split(vbCrLf)(0) 'Donc là ça veut dire qu'on a un signal en entier donc
    '        TempString = "" 'On reset la chaine temporaire
    '        TimerReception.Enabled = True 'Et on empêche de recevoir pendant un certain temps
    '    Else
    '        Exit Sub 'Et sinon ça veut dire qu'on a pas encore toute la chaîne donc on attend la suite
    '    End If
    '    If CaptureGraphique Then 'Ca c'est si on a l'UI de création de contrôle, on envoie le signal dans la listbox et on l'interprète pas
    '        If AjouterLigne.listTouches.Items.IndexOf(myString) = -1 Then AjouterLigne.listTouches.Items.Add(myString)
    '        txtIn.Text += "[" & Heure() & "] [Réception COM] Signal capturé (interface graphique) : " & myString & vbCrLf
    '        Exit Sub
    '    ElseIf WaitingCapture Then 'Pareil qu'avant, sauf que là le signal on l'ajoute au fichier de config sur la droite de la fenêtre
    '        If tConfig.Text = "" Then
    '            tConfig.Text &= myString & ";"
    '        Else
    '            tConfig.Text &= vbCrLf & myString & ";"
    '        End If
    '        txtIn.Text += "[" & Heure() & "] [Réception COM] Signal capturé : " & myString & vbCrLf
    '        WaitingCapture = False
    '        Exit Sub
    '    End If

    '    If myString = "" Then Exit Sub

    '    Dim index As Integer = -1
    '    For i = 0 To ListSignaux.Count - 1 'Là, on analyse chaque signal de la liste et on regarde si il correspond à celui reçu. Ca peut paraître compliqué, la ligne du dessous, mais dites vous que c'est juste pour éviter de faire un Split(",") et un For-ception
    '        If (ListSignaux(i).StartsWith(myString & ",")) Or (ListSignaux(i).ToString.Contains("," & myString & ",")) Or (ListSignaux(i).ToString.EndsWith("," & myString)) Or (ListSignaux(i).ToString.Split(",").Length = 1 And ListSignaux(i) = myString) Then
    '            index = i
    '            Exit For
    '        End If
    '    Next
    '    If index = -1 Then 'Dans ce cas là, le signal est pas dans la liste
    '        txtIn.Text += "[" & Heure() & "] [Réception COM] Signal non reconnu : " & myString & vbCrLf
    '        LastTypeAction = "Signal"
    '        LastAction = "non reconnu"
    '    Else 'Sinon on va s'amuser à regarder à quoi il correspond
    '        txtIn.Text += "[" & Heure() & "] [Réception COM] Signal reconnu : " & myString & " | Action : [" & ListTypesActions(index) & "|" & ListActions(index) & "]" & vbCrLf
    '        Dim TypeAction As String = ListTypesActions(index).ToLower
    '        Dim Action As String = ListActions(index).ToLower
    '        Select Case TypeAction 'Selection du type d'action
    '            Case "cmd", "commande" 'Puis sélection de l'action
    '                LastTypeAction = "Commande"
    '                LastAction = Action
    '                Shell("cmd.exe /C " & Action)
    '            Case "vlc" 'Oui bah j'ai pas encore trouvé comment faire
    '            Case "sound", "son"
    '                LastTypeAction = "Son"
    '                Select Case Action
    '                    Case "moins", "minus", "diminuer", "baisser", "decrease", "dec", "-", "dim"
    '                        LastAction = "Diminuer"
    '                        Shell(NirCMDName & " changesysvolume -1000")
    '                    Case "plus", "augmenter", "increase", "raise", "inc", "aug", "+"
    '                        LastAction = "Augmenter"
    '                        Shell(NirCMDName & " changesysvolume 1000")
    '                    Case "mutetoggle", "togglemute", "toggle"
    '                        Shell(NirCMDName & " mutesysvolume 2")
    '                        LastAction = "Changer muet"
    '                    Case "mute", "muet", "off"
    '                        LastAction = "Muet"
    '                        Shell(NirCMDName & " mutesysvolume 1")
    '                    Case "nomute", "nonmute", "demute", "nonmuet", "pasmuet", "on"
    '                        LastAction = "Non muet"
    '                        Shell(NirCMDName & " mutesysvolume 0")
    '                End Select
    '            Case "others"
    '                Select Case Action
    '                    Case "screenshot", "capture", "captureecran", "shot", "screenshotpaint", "capturepaint", "captureecranpaint", "shotpaint"
    '                        LastTypeAction = "Screenshot"
    '                        LastAction = "Enregistre."
    '                        If LastSecond = DateTime.Now.Second Then
    '                            CompteurSeconde += 1
    '                        Else
    '                            CompteurSeconde = 1
    '                            LastSecond = DateTime.Now.Second
    '                        End If
    '                        Dim nomScreenshot As String = dossierScreenshots & DateHeure() & "_" & CompteurSeconde
    '                        Shell(NirCMDName & " cmdwait 0 savescreenshot """ & nomScreenshot & ".png""")
    '                        If Action.Contains("paint") Then
    '                            Shell("mspaint """ & nomScreenshot & ".png""", AppWinStyle.MaximizedFocus)
    '                            LastTypeAction = "+Paint"
    '                        End If
    '                End Select
    '            Case "kill", "terminate", "end", "killprocess", "terminer", "fermer"
    '                Shell(NirCMDName & " killprocess " & Action)
    '                LastTypeAction = "Term. process."
    '                LastAction = Action
    '            Case "launch", "lancer", "execute", "start"
    '                LastTypeAction = "Lancer"
    '                LastAction = Action
    '                Process.Start(Action)
    '            Case "power", "alimentation"
    '                LastTypeAction = "Alimentation"
    '                Dim add As String = "!"
    '                If Not Action.Contains("!") Then
    '                    add = Nothing
    '                End If
    '                Select Case Action
    '                    Case "logoff", "deco", "deconnexion", "deconnecter", "logoff!", "deco!", "deconnexion!", "deconnecter!"
    '                        Shell(WizmoName & " -logoff" & add)
    '                        LastAction = "Deconnexion"
    '                    Case "hibernate", "hibernation", "veilleprolongee", "prolongee", "hibernate!", "hibernation!", "veilleprolongee!", "prolongee!"
    '                        Shell(WizmoName & " -hibernate" & add)
    '                        LastAction = "Hibernation"
    '                    Case "verrouiller", "lock", "verrouiller!", "lock!"
    '                        Shell(WizmoName & " -lock" & add)
    '                        LastAction = "Verrouillage"
    '                    Case "shutdown", "eteindre", "shutdown!", "eteindre!"
    '                        Shell(WizmoName & " -shutdown" & add)
    '                        LastAction = "Extinction"
    '                    Case "redemarrer", "reboot", "reboot!", "redemarrer!"
    '                        Shell(WizmoName & " -reboot" & add)
    '                        LastAction = "Redémarrage"
    '                    Case "veille", "standby", "veille!", "standby!"
    '                        Shell(WizmoName & " -standby" & add)
    '                        LastAction = "Veille"
    '                    Case "screenoff", "monoff", "monitoroff", "eteindreecran", "ecranoff", "screenoff!", "monoff!", "monitoroff!", "eteindreecran!", "ecranoff!"
    '                        Shell(WizmoName & " -monoff" & add)
    '                        LastAction = "Ecran off"
    '                    Case Else
    '                        LastAction = "Inconnu"
    '                        txtIn.Text += "[" & Heure() & "] [Power Control] Action non reconnue." & vbCrLf
    '                End Select
    '            Case "itunes"
    '                LastTypeAction = "iTunes"
    '                If TryCreateiTunesObject() = False Then
    '                    txtIn.Text += "[" & Heure() & "] [iTunes Ctrl] Aucune instance d'iTunes détectée." & vbCrLf
    '                    LastAction = "iTunes ferme"
    '                    SendSerialData(LastTypeAction, LastAction, False) 'On envoie à l'Arduino l'ordre d'afficher l'action et le type d'action
    '                    Exit Sub
    '                End If
    '                Select Case Action.ToLower
    '                    Case "pause", "p"
    '                        iTunesObject.pauseTrack()
    '                        MessageBox.Show(iTunesObject.getTrack())
    '                        LastAction = "Pause"
    '                    Case "play", "lecture"
    '                        iTunesObject.playTrack()
    '                        LastAction = "Play"
    '                    Case "playpause"
    '                        iTunesObject.playPause()
    '                        LastAction = "Play / Pause"
    '                        MessageBox.Show(iTunesObject.getTrack())
    '                    Case "stop"
    '                        iTunesObject.stopTrack()
    '                        LastAction = "Stop"
    '                    Case "next", "suivant"
    '                        iTunesObject.nextTrack()
    '                        LastAction = "Suivant"
    '                    Case "prev", "previous", "precedent"
    '                        iTunesObject.previousTrack()
    '                        LastAction = "Precedent"
    '                    Case "moins", "minus", "diminuer", "baisser", "decrease", "dec", "-"
    '                        iTunesObject.volumeDec()
    '                        LastAction = "Volume -"
    '                    Case "+", "plus"
    '                        iTunesObject.volumeInc()
    '                        LastAction = "Volume +"
    '                    Case Else
    '                        LastAction = "Inconnu"
    '                End Select
    '            Case Else
    '                LastTypeAction = "Type d'action"
    '                LastAction = "non reconnu"
    '        End Select
    '    End If
    '    SendSerialData(LastTypeAction, LastAction, False) 'On envoie à l'Arduino l'ordre d'afficher l'action et le type d'action
    '    PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, LastTypeAction, LastAction) 'Et on actualise le panneau de visualisation
    'End Sub
End Module
