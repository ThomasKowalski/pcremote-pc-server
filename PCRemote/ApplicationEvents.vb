﻿Namespace My

    ' Les événements suivants sont disponibles pour MyApplication :
    ' 
    ' Startup : déclenché au démarrage de l'application avant la création du formulaire de démarrage.
    ' Shutdown : déclenché après la fermeture de tous les formulaires de l'application. Cet événement n'est pas déclenché si l'application se termine de façon anormale.
    ' UnhandledException : déclenché si l'application rencontre une exception non gérée.
    ' StartupNextInstance : déclenché lors du lancement d'une application à instance unique et si cette application est déjà active. 
    ' NetworkAvailabilityChanged : déclenché lorsque la connexion réseau est connectée ou déconnectée.
    Partial Friend Class MyApplication

        Private Sub MyApplication_Shutdown(sender As Object, e As EventArgs) Handles Me.Shutdown
            PanelVisualisation.IconeNotification.Visible = False
            'En fait... cet évènement ne fonctionne pas. Ce qui est plutôt ballot quand même. Mais bon ça fait bien pour la forme de l'écrire ici aussi.
        End Sub

        Private Sub MyApplication_UnhandledException(sender As Object, e As ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException
            'Si jamais y'a une erreur non gérée, on propose à l'utilisateur de nous envoyer un rapport en lui faisant, en plus, une grande référence au Grand François Pérusse.
            InputBox("Désolé, mais l'application a quitté inopinément le système suite à une erreur numéro 72." & vbCrLf & "Non, en fait, on a aucune idée de ce qu'il s'est passé." & vbCrLf & "Si vous voulez nous aider, rendez-vous sur thomaskowalski.net/contact et envoyez-nous les circonstances où sont arrivées l'erreur, avec le texte qui s'affiche ci-dessous :", "Oups.", e.Exception.Message)
        End Sub
    End Class


End Namespace

