﻿Public Class ParametersManager
    Public Property Demarrage As Boolean = False
    Public Property GraphicMode As Boolean = False
    Public Property DisplayLog As Boolean = False
    Public Property ScreenshotsFolder As String = ""
    Public Property NameToDisplay As String = ""
    Public Property TopMost As Boolean = True

    Public Sub New()
        Demarrage = Registry.LancementDemarrage
        GraphicMode = GetParameter("GraphicMode", "True")
        DisplayLog = GetParameter("DisplayLog", "True")
        TopMost = GetParameter("TopMost", "True")
        Debug.Print("TopMost : " & Me.TopMost)
        ScreenshotsFolder = GetScreenshotsFolder()
        NameToDisplay = GetNameToDisplay()
        CreerDossierConfig()
    End Sub

    Private Sub CreerDossierConfig()
        If Not My.Computer.FileSystem.DirectoryExists(Environ("APPDATA") & "\PCRemote\") Then My.Computer.FileSystem.CreateDirectory(Environ("APPDATA") & "\PCRemote\")
    End Sub

    Public Function ChargerActionsPossibles() As List(Of String)
        Dim Returned As New List(Of String)

        Returned.Add("cmd")

        Returned.Add("kill")
        Returned.Add("terminate")
        Returned.Add("end")
        Returned.Add("killprocess")
        Returned.Add("terminer")
        Returned.Add("fermer")

        Returned.Add("launch")
        Returned.Add("execute")
        Returned.Add("lancer")
        Returned.Add("start")

        Return Returned
    End Function
    Public Function ChargerActionsPossiblesAvecParametres() As List(Of String)
        Dim Returned As New List(Of String)

        Returned.Add("itunes,play")
        Returned.Add("itunes,pause")
        Returned.Add("itunes,playpause")
        Returned.Add("itunes,stop")
        Returned.Add("itunes,prev")
        Returned.Add("itunes,precedent")
        Returned.Add("itunes,next")
        Returned.Add("itunes,suivant")
        Returned.Add("itunes,suiv")
        Returned.Add("itunes,moins")
        Returned.Add("itunes,plus")
        Returned.Add("itunes,volume+")
        Returned.Add("itunes,volume-")
        Returned.Add("itunes,minus")

        Returned.Add("sound,moins")
        Returned.Add("sound,minus")
        Returned.Add("sound,diminuer")
        Returned.Add("sound,baisser")
        Returned.Add("sound,decrease")
        Returned.Add("sound,dec")
        Returned.Add("sound,dim")
        Returned.Add("sound,-")
        Returned.Add("sound,plus")
        Returned.Add("sound,augmenter")
        Returned.Add("sound,increase")
        Returned.Add("sound,raise")
        Returned.Add("sound,inc")
        Returned.Add("sound,aug")
        Returned.Add("sound,+")
        Returned.Add("sound,mutetoggle")
        Returned.Add("sound,togglemute")
        Returned.Add("sound,toggle")
        Returned.Add("sound,off")
        Returned.Add("sound,muet")
        Returned.Add("sound,mute")
        Returned.Add("sound,nomute")
        Returned.Add("sound,nonmute")
        Returned.Add("sound,demute")
        Returned.Add("sound,nonmuet")
        Returned.Add("sound,pasmuet")
        Returned.Add("sound,on")

        Returned.Add("power,logoff")
        Returned.Add("power,deco")
        Returned.Add("power,deconnexion")
        Returned.Add("power,deconnecter")
        Returned.Add("power,logoff!")
        Returned.Add("power,deconnexion!")
        Returned.Add("power,deconnecter!")
        Returned.Add("power,hibernate!")
        Returned.Add("power,hibernation!")
        Returned.Add("power,veilleprolongee!")
        Returned.Add("power,prolongee!")
        Returned.Add("power,hibernate")
        Returned.Add("power,hibernation")
        Returned.Add("power,veilleprolongee")
        Returned.Add("power,prolongee")
        Returned.Add("power,lock")
        Returned.Add("power,verrouiller")
        Returned.Add("power,lock!")
        Returned.Add("power,verrouiller!")
        Returned.Add("power,shutdown")
        Returned.Add("power,eteindre")
        Returned.Add("power,shutdown!")
        Returned.Add("power,eteindre!")
        Returned.Add("power,reboot")
        Returned.Add("power,redemarrer")
        Returned.Add("power,reboot!")
        Returned.Add("power,redemarrer!")
        Returned.Add("power,veille")
        Returned.Add("power,standby")
        Returned.Add("power,veille!")
        Returned.Add("power,standby!")
        Returned.Add("power,screenoff")
        Returned.Add("power,monoff")
        Returned.Add("power,monitoroff")
        Returned.Add("power,eteindreecran")
        Returned.Add("power,ecranoff")
        Returned.Add("power,screenoff!")
        Returned.Add("power,monoff!")
        Returned.Add("power,monitoroff!")
        Returned.Add("power,eteindreecran!")
        Returned.Add("power,ecranoff!")

        Returned.Add("others,screenshot")
        Returned.Add("others,capture")
        Returned.Add("others,captureecran")
        Returned.Add("others,shot")
        Returned.Add("others,screenshotpaint")
        Returned.Add("others,capturepaint")
        Returned.Add("others,captureecranpaint")
        Returned.Add("others,shotpaint")

        Return Returned
    End Function

    Private Function GetScreenshotsFolder() As String 'Récupère le dossier des captures d'écran
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ScreenshotsFolder", "") = "" Then
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ScreenshotsFolder", Environ("USERPROFILE") & "\PCRemote Screenshots")
            If My.Computer.FileSystem.DirectoryExists(Environ("USERPROFILE") & "\PCRemote Screenshots") = False Then My.Computer.FileSystem.CreateDirectory(Environ("USERPROFILE") & "\PCRemote Screenshots")
            Return GetScreenshotsFolder()
        Else
            Return My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ScreenshotsFolder", "") & "\".Replace("\\", "\")
        End If
    End Function

    Private Function GetNameToDisplay() 'Récupère le nom à afficher sur l'Arduino lors de la connexion
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ComputerName", "") = "" Then
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ComputerName", Environment.MachineName)
            Return GetNameToDisplay()
        Else
            Return My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "ComputerName", "")
        End If
    End Function

    Public Sub ChangeParameter(name As String, value As String)
        Debug.Print("Parameter changed : " & name & " / " & value)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", name, value)
    End Sub

    Public Function GetParameter(name As String, defaultValue As String) As String
        Dim data As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", name, defaultValue)
        If (data = "") Then
            ChangeParameter(name, defaultValue)
            Return GetParameter(name, defaultValue)
        End If
        Return data
    End Function
End Class
