﻿Public Class NirCMD
    Private Property path As String
    Public Sub New()
        Dim ms As IO.MemoryStream
        Dim fs As IO.FileStream
        If Environment.Is64BitOperatingSystem Then
            ms = New IO.MemoryStream(My.Resources.nircmd64)
            fs = IO.File.OpenWrite(Environ("APPDATA") & "\PCRemote\" & "\nircmd.exe")
            ms.WriteTo(fs)
            fs.Close()
            ms = New IO.MemoryStream(My.Resources.nircmdc64)
            fs = IO.File.OpenWrite(Environ("APPDATA") & "\PCRemote\" & "\nircmdc.exe")
            ms.WriteTo(fs)
            fs.Close()
        Else
            ms = New IO.MemoryStream(My.Resources.nircmd32)
            fs = IO.File.OpenWrite(Environ("APPDATA") & "\PCRemote\" & "\nircmd.exe")
            ms.WriteTo(fs)
            fs.Close()
            ms = New IO.MemoryStream(My.Resources.nircmdc32)
            fs = IO.File.OpenWrite(Environ("APPDATA") & "\PCRemote\" & "\nircmdc.exe")
            ms.WriteTo(fs)
            fs.Close()
        End If
        'Copie des dépendances
        Me.path = Environ("APPDATA") & "\PCRemote\" & "\nircmd.exe"
    End Sub
    Public Sub CallAction(parameter As String)
        Shell(path & " " & parameter)
    End Sub
    Public Sub Delete()
        Try
            My.Computer.FileSystem.DeleteFile(path, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
            My.Computer.FileSystem.DeleteFile(path.Replace(".exe", "c.exe"), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        Catch ex As Exception

        End Try
    End Sub
End Class