﻿Public Class LogProvider
    Public Sub New()

    End Sub
    Public Sub WriteLog(contentToAdd As String)
        Dim ContenuLog As String
        If My.Computer.FileSystem.FileExists(Environ("APPDATA") & "\PCRemote\" & "\pcremote-" & DateTime.Now.ToString("yyyy-MM-dd") & ".log") Then
            Dim readLog As New IO.StreamReader(Environ("APPDATA") & "\PCRemote\" & "\pcremote-" & DateTime.Now.ToString("yyyy-MM-dd") & ".log", System.Text.Encoding.UTF8)
            ContenuLog = readLog.ReadToEnd
            readLog.Close()
        Else
            ContenuLog = ""
        End If
        Dim writeLog As New IO.StreamWriter(Environ("APPDATA") & "\PCRemote\" & "\pcremote-" & DateTime.Now.ToString("yyyy-MM-dd") & ".log", False)
        If ContenuLog = "" Then
            writeLog.Write(contentToAdd & "-----------------------------------------------------------")
            writeLog.Close()
        Else
            writeLog.Write(ContenuLog & contentToAdd & "-----------------------------------------------------------")
            writeLog.Close()
        End If
    End Sub
End Class
