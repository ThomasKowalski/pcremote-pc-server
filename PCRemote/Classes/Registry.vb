﻿Public Module Registry
    Public Sub AddStartup(ByVal Name As String, ByVal Path As String) 'Fait que le programme se lance au démarrage
        Static Dim Registry As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Dim Key As Microsoft.Win32.RegistryKey = Registry.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
        Key.SetValue(Name, Path, Microsoft.Win32.RegistryValueKind.String)
    End Sub
    Public Sub RemoveStartup(ByVal Name As String) 'Fait que le programme ne se lance pas au démarrage
        Dim Registry As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Dim Key As Microsoft.Win32.RegistryKey = Registry.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
        Key.DeleteValue(Name)
    End Sub
    Public Function LancementDemarrage() 'Vérifie si le programme se lance au démarrage
        Dim Registry As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Dim Key As Microsoft.Win32.RegistryKey = Registry.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
        If Key.GetValue("PC Remote", "") = "" Then
            Return False
        Else
            Return True
        End If
    End Function
End Module
