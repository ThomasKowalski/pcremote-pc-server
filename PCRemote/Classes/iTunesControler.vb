﻿'********************************************************************************************
'Classe iTunesControler
'Il s'agit de l'objet que l'on utilisera pour contrôler iTunes. Rien de plus
'Je ne vais pas détailler l'utilisation de celui-ci puisqu'elle est relativement simple
'Cet objet nécessite les références mises dans... les références du projet

Imports iTunesLib
Imports ITDETECTORLib
Imports System.Threading
'Imports Microsoft.VisualBasic.Compatibility.VB6
Public Class iTunesControler

    Structure idTags
        '
        ' Structure to store details in listviewitem tag, not all needed
        ' just added all in case they came in useful.
        '
        Dim SourceID As Integer
        Dim PlayListID As Integer
        Dim TrackID As Integer
        Dim TrackDatabaseID As Integer
        Public Sub New(ByVal SourceID As Integer, ByVal PlayListID As Integer, ByVal TrackID As Integer, ByVal TrackDatabaseID As Integer)
            Me.SourceID = SourceID
            Me.PlayListID = PlayListID
            Me.TrackID = TrackID
            Me.TrackDatabaseID = TrackDatabaseID
        End Sub
    End Structure
    '
    ' Columnsorter for listview
    '
    'Private lvwColumnSorter As ListViewColumnSorter = New ListViewColumnSorter()
    Dim ctrack As String = ""
    '
    ' Main iTunes objects
    '
    Dim WithEvents iTunesApp As iTunesLib.iTunesApp = Nothing
    Dim mainLibrary As IITSource
    Dim LibPlaylists As IITPlaylistCollection
    Dim COMStateActive As Boolean = True

    Sub New()
        Try
            iTunesApp = New iTunesLib.iTunesApp
        Catch ex As Exception
            MsgBox("Problème avec iTunes : " + vbCrLf + ex.Message)
        End Try
    End Sub

    Public Sub nextTrack()
        iTunesApp.NextTrack()
    End Sub
    Public Sub previousTrack()
        iTunesApp.PreviousTrack()
    End Sub
    Public Sub pauseTrack()
        iTunesApp.Pause()
    End Sub
    Public Sub playTrack()
        iTunesApp.Play()
    End Sub
    Public Sub playPause()
        iTunesApp.PlayPause()
    End Sub
    Public Sub stopTrack()
        iTunesApp.Stop()
    End Sub
    Public Sub mute()
        iTunesApp.Mute = True
    End Sub
    Public Sub volumeInc()
        iTunesApp.SoundVolume += 5
    End Sub
    Public Sub volumeDec()
        iTunesApp.SoundVolume -= 5
    End Sub
End Class
