﻿Public Class ConfigurationParser

    Private Property TranslationDictionnary As Dictionary(Of String, String)
    Public Sub New()
        TranslationDictionnary = GenerateTranslationDictionnary()
    End Sub
    Public Function Normalize(toNormalize As String) As String
        toNormalize = toNormalize.Replace(vbCrLf & vbCrLf, vbCrLf)
        While toNormalize.Contains(",,") Or toNormalize.Contains(",;")
            toNormalize = toNormalize.Replace(",,", ",")
            toNormalize = toNormalize.Replace(",;", ";")
        End While
        Return toNormalize
    End Function

    Public Function Analyze(toAnalyze As String) As Object()
        Dim tempTableau As String() = toAnalyze.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        Dim ListID As New List(Of String)
        Dim ListNoms As New Dictionary(Of String, String)
        Dim ListActions As New Dictionary(Of String, String)
        Dim ListSignaux As New Dictionary(Of String, String)
        For i = 0 To tempTableau.Length - 1
            If Not tempTableau(i).StartsWith("//") Then
                Dim tempTableauInline As String() = tempTableau(i).Split(";")
                Dim ID = tempTableauInline(0)
                ListID.Add(ID)
                ListNoms.Add(ID, tempTableauInline(1))
                ListSignaux.Add(ID, tempTableauInline(2))
                ListActions.Add(ID, tempTableauInline(3))
            End If
        Next
        Return {ListID, ListNoms, ListSignaux, ListActions}
    End Function
    Public Enum ParseError
        NoConfiguration
        GenericError
        NoID
        NoName
        NoKey
        NoCommand
        NoCategory
        NoParameter
        BadKeyDeclaration
        KeyAlreadyUsed
        IDAlreadyUsed
        BadCommaNumber
        BadAction
        Success
    End Enum
    Public Function Parse(Config As String) As Object()
        If Config = "" Then
            Return {ParseError.NoConfiguration, 0}
        End If
        Dim TableauLignes As String() = Config.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        Dim ListeTouchesDejaAttitrees As New List(Of Integer)
        Dim ListeIDDejaAttitres As New List(Of String)
        For i = 0 To TableauLignes.Count - 1
            If TableauLignes(i) <> vbLf And TableauLignes(i) <> "" And TableauLignes(i).StartsWith("//") = False Then
                Dim TableauContenuLigne As String() = TableauLignes(i).Split(";")
                Dim ID As String
                Dim Nom As String
                Dim Touches As String
                Dim Commandes As String
                Try
                    ID = TableauContenuLigne(0)
                    Nom = TableauContenuLigne(1)
                    Touches = TableauContenuLigne(2)
                    Commandes = TableauContenuLigne(3)
                Catch ex As Exception
                    Return {ParseError.GenericError, i + 1}
                End Try
                If ID = Nothing Or ID = "" Then
                    Return {ParseError.NoID, i + 1}
                ElseIf Nom = Nothing Or Nom = "" Then
                    Return {ParseError.NoName, i + 1}
                ElseIf Touches = Nothing Or Touches = "" Then
                    Return {ParseError.NoKey, i + 1}
                ElseIf Commandes = Nothing Or Commandes = "" Then
                    Return {ParseError.NoCommand, i + 1}
                End If
                If ListeIDDejaAttitres.Contains(ID) Then
                    Return {ParseError.IDAlreadyUsed, i + 1}
                Else
                    ListeIDDejaAttitres.Add(ID)
                End If
                'Analyse des touches
                Dim TableauTouchesLigne As String() = Touches.Split(",")
                For Each Touche In TableauTouchesLigne
                    Dim IntegerTouche As Integer
                    If Touche = "" Then
                        Return {ParseError.NoKey, i + 1}
                    ElseIf Integer.TryParse(Touche, IntegerTouche) = Nothing Then
                        Return {ParseError.BadKeyDeclaration, i + 1}
                    Else
                        If ListeTouchesDejaAttitrees.Contains(Touche) Then
                            Return {ParseError.KeyAlreadyUsed, i + 1}
                        Else
                            ListeTouchesDejaAttitrees.Add(Touche)
                        End If
                    End If
                Next
                'Vérification des trois paramètres dans la ligne
                If TableauContenuLigne.Count <> 4 Then
                    Return {ParseError.BadCommaNumber, i + 1}
                End If
                Dim Correct As Boolean = False
                Dim TableauCommandes = Commandes.Split("|")
                For Each Action In TableauCommandes
                    For Each PossibleAction In ListeActionsPossibles
                        If Action = PossibleAction Then
                            Correct = True
                            Exit For
                        End If
                    Next
                    If Correct = False Then
                        For Each PossibleAction In ListeActionsPossiblesAvecParametres
                            If PossibleAction.StartsWith(Action.Split(",")(0)) Then
                                Correct = True
                                Exit For
                            End If
                        Next
                    End If
                    If Not Correct Then
                        Return {ParseError.BadAction, i + 1}
                        Exit Function
                    End If
                Next
            End If
        Next
        Return {ParseError.Success, 0}
    End Function
    Public Function Translate(toTranslate As String) As String
        Try
            Return TranslationDictionnary(toTranslate)
        Catch ex As Exception
            If toTranslate = "" Then Return "" Else Debug.Print(toTranslate)
        End Try
    End Function
    Public Function getCommandFromTranslation(toTranslate As String) As String
        For Each item In TranslationDictionnary
            If item.Value = toTranslate Then
                Return item.Key
            End If
        Next
        If toTranslate = "" Then Return "" Else Return "Invalide"
    End Function
    Public Function GenerateTranslationDictionnary() As Dictionary(Of String, String)
        Dim Returned As New Dictionary(Of String, String)
        Returned.Add("itunes,play", "iTunes / Play")
        Returned.Add("itunes,pause", "iTunes / Pause")
        Returned.Add("itunes,playpause", "iTunes / Play-Pause")
        Returned.Add("itunes,stop", "iTunes / Stop")
        Returned.Add("itunes,prev", "iTunes / Précédent")
        Returned.Add("itunes,precedent", "iTunes / Précédent")
        Returned.Add("itunes,next", "iTunes / Suivant")
        Returned.Add("itunes,suivant", "iTunes / Suivant")
        Returned.Add("itunes,suiv", "iTunes / Suivant")
        Returned.Add("itunes,moins", "iTunes / [-] Volume")
        Returned.Add("itunes,plus", "iTunes / [+] Volume")
        Returned.Add("itunes,volume+", "iTunes / [+] Volume")
        Returned.Add("itunes,volume-", "iTunes / [-] Volume")
        Returned.Add("itunes,minus", "iTunes / [-] Volume")

        Returned.Add("sound,moins", "[-] Volume")
        Returned.Add("sound,minus", "[-] Volume")
        Returned.Add("sound,diminuer", "[-] Volume")
        Returned.Add("sound,baisser", "[-] Volume")
        Returned.Add("sound,decrease", "[-] Volume")
        Returned.Add("sound,dec", "[-] Volume")
        Returned.Add("sound,dim", "[-] Volume")
        Returned.Add("sound,-", "[-] Volume")
        Returned.Add("sound,plus", "[+] Volume")
        Returned.Add("sound,augmenter", "[+] Volume")
        Returned.Add("sound,increase", "[+] Volume")
        Returned.Add("sound,raise", "[+] Volume")
        Returned.Add("sound,inc", "[+] Volume")
        Returned.Add("sound,aug", "[+] Volume")
        Returned.Add("sound,+", "[+] Volume")
        Returned.Add("sound,mutetoggle", "Inverser muet")
        Returned.Add("sound,togglemute", "Inverser muet")
        Returned.Add("sound,toggle", "Inverser muet")
        Returned.Add("sound,off", "Muet")
        Returned.Add("sound,muet", "Muet")
        Returned.Add("sound,mute", "Muet")
        Returned.Add("sound,nomute", "Non muet")
        Returned.Add("sound,nonmute", "Non muet")
        Returned.Add("sound,demute", "Non muet")
        Returned.Add("sound,nonmuet", "Non muet")
        Returned.Add("sound,pasmuet", "Non muet")
        Returned.Add("sound,on", "Non muet")

        Returned.Add("power,logoff", "Déconnexion")
        Returned.Add("power,deco", "Déconnexion")
        Returned.Add("power,deconnexion", "Déconnexion")
        Returned.Add("power,deconnecter", "Déconnexion")
        Returned.Add("power,logoff!", "Déconnexion !")
        Returned.Add("power,deconnexion!", "Déconnexion !")
        Returned.Add("power,deconnecter!", "Veille prolongée !")
        Returned.Add("power,hibernate!", "Veille prolongée !")
        Returned.Add("power,hibernation!", "Veille prolongée !")
        Returned.Add("power,veilleprolongee!", "Veille prolongée !")
        Returned.Add("power,prolongee!", "Veille prolongée !")
        Returned.Add("power,hibernate", "Veille prolongée")
        Returned.Add("power,hibernation", "Veille prolongée")
        Returned.Add("power,veilleprolongee", "Veille prolongée")
        Returned.Add("power,prolongee", "Veille prolongée")
        Returned.Add("power,lock", "Verrouiller")
        Returned.Add("power,verrouiller", "Verrouiller")
        Returned.Add("power,lock!", "Verrouiller !")
        Returned.Add("power,verrouiller!", "Verrouiler !")
        Returned.Add("power,shutdown", "Arrêt")
        Returned.Add("power,eteindre", "Arrêt")
        Returned.Add("power,shutdown!", "Arrêt !")
        Returned.Add("power,eteindre!", "Arrêt !")
        Returned.Add("power,reboot", "Redémarrage")
        Returned.Add("power,redemarrer", "Redémarrage")
        Returned.Add("power,reboot!", "Redémarrage !")
        Returned.Add("power,redemarrer!", "Redémarrage !")
        Returned.Add("power,veille", "Mise en veille")
        Returned.Add("power,standby", "Mise en veille")
        Returned.Add("power,veille!", "Mise en veille !")
        Returned.Add("power,standby!", "Mise en veille !")
        Returned.Add("power,screenoff", "Extinction d'écran")
        Returned.Add("power,monoff", "Extinction d'écran")
        Returned.Add("power,monitoroff", "Extinction d'écran")
        Returned.Add("power,eteindreecran", "Extinction d'écran")
        Returned.Add("power,ecranoff", "Extinction d'écran")
        Returned.Add("power,screenoff!", "Extinction d'écran !")
        Returned.Add("power,monoff!", "Extinction d'écran !")
        Returned.Add("power,monitoroff!", "Extinction d'écran !")
        Returned.Add("power,eteindreecran!", "Extinction d'écran !")
        Returned.Add("power,ecranoff!", "Extinction d'écran !")

        Returned.Add("others,screenshot", "ScreenShot")
        Returned.Add("others,capture", "Screenshot")
        Returned.Add("others,captureecran", "Screenshot")
        Returned.Add("others,shot", "Screenshot + Edition")
        Returned.Add("others,screenshotpaint", "Screenshot + Edition")
        Returned.Add("others,capturepaint", "Screenshot + Edition")
        Returned.Add("others,captureecranpaint", "Screenshot + Edition")
        Returned.Add("others,shotpaint", "Screenshot + Edition")
        Return Returned
    End Function
    Public Function GoodTranslationDictionnary() As Dictionary(Of String, String)
        Dim Returned As New Dictionary(Of String, String)
        Returned.Add("itunes,play", "iTunes / Play")
        Returned.Add("itunes,pause", "iTunes / Pause")
        Returned.Add("itunes,playpause", "iTunes / Play-Pause")
        Returned.Add("itunes,stop", "iTunes / Stop")
        Returned.Add("itunes,prev", "iTunes / Précédent")
        Returned.Add("itunes,next", "iTunes / Suivant")
        Returned.Add("itunes,volume+", "iTunes / [+] Volume")
        Returned.Add("itunes,volume-", "iTunes / [-] Volume")

        Returned.Add("sound,-", "[-] Volume")
        Returned.Add("sound,+", "[+] Volume")
        Returned.Add("sound,toggle", "Inverser muet")
        Returned.Add("sound,mute", "Muet")
        Returned.Add("sound,demute", "Non muet")

        Returned.Add("power,logoff", "Déconnexion")
        Returned.Add("power,logoff!", "Déconnexion !")
        Returned.Add("power,hibernate!", "Veille prolongée !")
        Returned.Add("power,hibernate", "Veille prolongée")
        Returned.Add("power,lock", "Verrouiller")
        Returned.Add("power,lock!", "Verrouiller !")
        Returned.Add("power,shutdown", "Arrêt")
        Returned.Add("power,shutdown!", "Arrêt !")
        Returned.Add("power,reboot", "Redémarrage")
        Returned.Add("power,reboot!", "Redémarrage !")
        Returned.Add("power,standby", "Mise en veille")
        Returned.Add("power,standby!", "Mise en veille !")
        Returned.Add("power,monoff", "Extinction d'écran")
        Returned.Add("power,monoff!", "Extinction d'écran !")

        Returned.Add("others,screenshot", "Screenshot")
        Returned.Add("others,screenshotpaint", "Screenshot + Edition")
        Return Returned
    End Function
End Class
