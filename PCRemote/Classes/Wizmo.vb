﻿Public Class Wizmo
    Private Property path As String
    Public Sub New()
        'Copie des dépendances
        Dim ms As New IO.MemoryStream(My.Resources.wizmo)
        Dim fs As IO.FileStream = IO.File.OpenWrite(Environ("APPDATA") & "\PCRemote\" & "\wizmo.exe")
        ms.WriteTo(fs)
        fs.Close()
        Me.path = Environ("APPDATA") & "\PCRemote\" & "\wizmo.exe" & " -quiet"
    End Sub
    Public Sub CallAction(parameter As String)
        Shell(path & " -" & parameter)
    End Sub
    Public Sub Delete()
        Try
            My.Computer.FileSystem.DeleteFile(path, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        Catch ex As Exception

        End Try
    End Sub
End Class
