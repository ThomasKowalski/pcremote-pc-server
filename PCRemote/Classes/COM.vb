﻿Public Class COM
    Public Function GetNames() As List(Of String)
        ' Add reference to System.Management.dll.
        Dim Returned As New List(Of String)
        Try
            Dim searcher As New ManagementObjectSearcher( _
           "root\cimv2", _
           "SELECT * FROM Win32_SerialPort")

            For Each queryObj As ManagementObject In searcher.Get()
                Returned.Add(queryObj("Name"))
            Next


        Catch err As ManagementException
            MessageBox.Show("Impossible de récupérer les ports : " & err.Message)
        End Try
        Return Returned
    End Function

    'Private Sub GetSerialPortNames()
    '    For Each Port In COMManager.GetNames()
    '        cmbPort.Items.Add(Port)
    '    Next
    'End Sub
End Class
