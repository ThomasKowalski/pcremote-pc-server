﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PanelVisualisation
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PanelVisualisation))
        Me.IconeNotification = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.MenuIcone = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ParamètresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PremierPlanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanneauDaffichageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblConnecte = New System.Windows.Forms.Label()
        Me.lblTitre = New System.Windows.Forms.Label()
        Me.lblLastAction = New System.Windows.Forms.Label()
        Me.lblPort = New System.Windows.Forms.Label()
        Me.MenuIcone.SuspendLayout()
        Me.SuspendLayout()
        '
        'IconeNotification
        '
        Me.IconeNotification.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.IconeNotification.ContextMenuStrip = Me.MenuIcone
        Me.IconeNotification.Text = "PCRemote est démarré." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Cliquez pour afficher la connexion." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.IconeNotification.Visible = True
        '
        'MenuIcone
        '
        Me.MenuIcone.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ParamètresToolStripMenuItem, Me.PremierPlanToolStripMenuItem, Me.PanneauDaffichageToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuIcone.Name = "MenuIcone"
        Me.MenuIcone.Size = New System.Drawing.Size(183, 92)
        '
        'ParamètresToolStripMenuItem
        '
        Me.ParamètresToolStripMenuItem.Name = "ParamètresToolStripMenuItem"
        Me.ParamètresToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ParamètresToolStripMenuItem.Text = "Fenêtre principale"
        '
        'PremierPlanToolStripMenuItem
        '
        Me.PremierPlanToolStripMenuItem.Checked = True
        Me.PremierPlanToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.PremierPlanToolStripMenuItem.Name = "PremierPlanToolStripMenuItem"
        Me.PremierPlanToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.PremierPlanToolStripMenuItem.Text = "Premier plan"
        '
        'PanneauDaffichageToolStripMenuItem
        '
        Me.PanneauDaffichageToolStripMenuItem.Checked = True
        Me.PanneauDaffichageToolStripMenuItem.CheckOnClick = True
        Me.PanneauDaffichageToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.PanneauDaffichageToolStripMenuItem.Name = "PanneauDaffichageToolStripMenuItem"
        Me.PanneauDaffichageToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.PanneauDaffichageToolStripMenuItem.Text = "Panneau d'affichage"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'lblConnecte
        '
        Me.lblConnecte.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.lblConnecte.ForeColor = System.Drawing.Color.White
        Me.lblConnecte.Location = New System.Drawing.Point(12, 36)
        Me.lblConnecte.Name = "lblConnecte"
        Me.lblConnecte.Size = New System.Drawing.Size(219, 23)
        Me.lblConnecte.TabIndex = 1
        Me.lblConnecte.Text = "Déconnecté"
        Me.lblConnecte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitre
        '
        Me.lblTitre.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitre.ForeColor = System.Drawing.Color.White
        Me.lblTitre.Location = New System.Drawing.Point(12, 9)
        Me.lblTitre.Name = "lblTitre"
        Me.lblTitre.Size = New System.Drawing.Size(222, 23)
        Me.lblTitre.TabIndex = 1
        Me.lblTitre.Text = "PC Remote"
        Me.lblTitre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastAction
        '
        Me.lblLastAction.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.lblLastAction.ForeColor = System.Drawing.Color.White
        Me.lblLastAction.Location = New System.Drawing.Point(13, 93)
        Me.lblLastAction.Name = "lblLastAction"
        Me.lblLastAction.Size = New System.Drawing.Size(218, 28)
        Me.lblLastAction.TabIndex = 1
        Me.lblLastAction.Text = "Dernière action"
        Me.lblLastAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPort
        '
        Me.lblPort.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.lblPort.ForeColor = System.Drawing.Color.White
        Me.lblPort.Location = New System.Drawing.Point(12, 68)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(219, 22)
        Me.lblPort.TabIndex = 1
        Me.lblPort.Text = "Port"
        Me.lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PanelVisualisation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(243, 132)
        Me.Controls.Add(Me.lblTitre)
        Me.Controls.Add(Me.lblConnecte)
        Me.Controls.Add(Me.lblPort)
        Me.Controls.Add(Me.lblLastAction)
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PanelVisualisation"
        Me.ShowInTaskbar = False
        Me.Text = "PCRemote"
        Me.TopMost = True
        Me.MenuIcone.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents IconeNotification As System.Windows.Forms.NotifyIcon
    Friend WithEvents MenuIcone As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ParamètresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanneauDaffichageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuitterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblConnecte As System.Windows.Forms.Label
    Friend WithEvents lblTitre As System.Windows.Forms.Label
    Friend WithEvents lblLastAction As System.Windows.Forms.Label
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents PremierPlanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
