﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AjouterLigne
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Explorateur Windows")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("iTunes")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Google Chrome")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Internet Explorer")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Mozilla Firefox")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Word")
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Excel")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("PowerPoint")
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("OneNote")
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Outlook")
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Personnalisé (programme / site Internet / fichier)")
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Lancer un programme", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4, TreeNode5, TreeNode6, TreeNode7, TreeNode8, TreeNode9, TreeNode10, TreeNode11})
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Play")
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Pause")
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Play / Pause")
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Stop")
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Suivant")
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Précédent")
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Volume -")
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Volume +")
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("iTunes", New System.Windows.Forms.TreeNode() {TreeNode13, TreeNode14, TreeNode15, TreeNode16, TreeNode17, TreeNode18, TreeNode19, TreeNode20})
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Veille")
        Dim TreeNode23 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Eteindre")
        Dim TreeNode24 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Redémarrer")
        Dim TreeNode25 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Déconnexion")
        Dim TreeNode26 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Verrouiller")
        Dim TreeNode27 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Eteindre l'écran")
        Dim TreeNode28 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Alimentation", New System.Windows.Forms.TreeNode() {TreeNode22, TreeNode23, TreeNode24, TreeNode25, TreeNode26, TreeNode27})
        Dim TreeNode29 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Play")
        Dim TreeNode30 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Pause")
        Dim TreeNode31 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Suivant")
        Dim TreeNode32 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Précédent")
        Dim TreeNode33 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Plein écran")
        Dim TreeNode34 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Volume +")
        Dim TreeNode35 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("VLC (non implémenté)", New System.Windows.Forms.TreeNode() {TreeNode29, TreeNode30, TreeNode31, TreeNode32, TreeNode33, TreeNode34})
        Dim TreeNode36 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Volume +")
        Dim TreeNode37 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Volume -")
        Dim TreeNode38 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Muet / Non muet")
        Dim TreeNode39 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Muet")
        Dim TreeNode40 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Non muet")
        Dim TreeNode41 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Son système", New System.Windows.Forms.TreeNode() {TreeNode36, TreeNode37, TreeNode38, TreeNode39, TreeNode40})
        Dim TreeNode42 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Capture d'écran")
        Dim TreeNode43 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Capture d'écran + édition")
        Dim TreeNode44 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Terminer processus")
        Dim TreeNode45 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Autres", New System.Windows.Forms.TreeNode() {TreeNode42, TreeNode43, TreeNode44})
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AjouterLigne))
        Me.treeActions = New System.Windows.Forms.TreeView()
        Me.listTouches = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bCapturer = New System.Windows.Forms.Button()
        Me.bSupprimer = New System.Windows.Forms.Button()
        Me.bValider = New System.Windows.Forms.Button()
        Me.personnalise = New System.Windows.Forms.TextBox()
        Me.bParcourir = New System.Windows.Forms.Button()
        Me.ToolTipInfo = New System.Windows.Forms.ToolTip(Me.components)
        Me.SelectionnerCustom = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'treeActions
        '
        Me.treeActions.Cursor = System.Windows.Forms.Cursors.Default
        Me.treeActions.HideSelection = False
        Me.treeActions.Location = New System.Drawing.Point(12, 32)
        Me.treeActions.Name = "treeActions"
        TreeNode1.Name = "explorer.exe"
        TreeNode1.Text = "Explorateur Windows"
        TreeNode2.Name = "itunes"
        TreeNode2.Text = "iTunes"
        TreeNode3.Name = "chrome"
        TreeNode3.Text = "Google Chrome"
        TreeNode4.Name = "iexplore"
        TreeNode4.Text = "Internet Explorer"
        TreeNode5.Name = "firefox"
        TreeNode5.Text = "Mozilla Firefox"
        TreeNode6.Name = "word"
        TreeNode6.Text = "Word"
        TreeNode7.Name = "excel"
        TreeNode7.Text = "Excel"
        TreeNode8.Name = "powerpoint"
        TreeNode8.Text = "PowerPoint"
        TreeNode9.Name = "onenote"
        TreeNode9.Text = "OneNote"
        TreeNode10.Name = "outlook"
        TreeNode10.Text = "Outlook"
        TreeNode11.Name = "custom"
        TreeNode11.Text = "Personnalisé (programme / site Internet / fichier)"
        TreeNode12.Name = "launch"
        TreeNode12.Text = "Lancer un programme"
        TreeNode13.Name = "play"
        TreeNode13.Text = "Play"
        TreeNode14.Name = "pause"
        TreeNode14.Text = "Pause"
        TreeNode15.Name = "playpause"
        TreeNode15.Text = "Play / Pause"
        TreeNode16.Name = "stop"
        TreeNode16.Text = "Stop"
        TreeNode17.Name = "next"
        TreeNode17.Text = "Suivant"
        TreeNode18.Name = "prev"
        TreeNode18.Text = "Précédent"
        TreeNode19.Name = "moins"
        TreeNode19.Text = "Volume -"
        TreeNode20.Name = "plus"
        TreeNode20.Text = "Volume +"
        TreeNode21.Name = "itunes"
        TreeNode21.Text = "iTunes"
        TreeNode22.Name = "standby"
        TreeNode22.Text = "Veille"
        TreeNode23.Name = "shutdown"
        TreeNode23.Text = "Eteindre"
        TreeNode24.Name = "reboot"
        TreeNode24.Text = "Redémarrer"
        TreeNode25.Name = "logoff"
        TreeNode25.Text = "Déconnexion"
        TreeNode26.Name = "lock"
        TreeNode26.Text = "Verrouiller"
        TreeNode27.Name = "screenoff"
        TreeNode27.Text = "Eteindre l'écran"
        TreeNode28.Name = "power"
        TreeNode28.Text = "Alimentation"
        TreeNode29.Name = "play"
        TreeNode29.Text = "Play"
        TreeNode30.Name = "pause"
        TreeNode30.Text = "Pause"
        TreeNode31.Name = "next"
        TreeNode31.Text = "Suivant"
        TreeNode32.Name = "previous"
        TreeNode32.Text = "Précédent"
        TreeNode33.Name = "fullscreen"
        TreeNode33.Text = "Plein écran"
        TreeNode34.Name = "plus"
        TreeNode34.Text = "Volume +"
        TreeNode35.Name = "vlc"
        TreeNode35.Text = "VLC (non implémenté)"
        TreeNode36.Name = "plus"
        TreeNode36.Text = "Volume +"
        TreeNode37.Name = "minus"
        TreeNode37.Text = "Volume -"
        TreeNode38.Name = "mutetoggle"
        TreeNode38.Text = "Muet / Non muet"
        TreeNode39.Name = "mute"
        TreeNode39.Text = "Muet"
        TreeNode40.Name = "demute"
        TreeNode40.Text = "Non muet"
        TreeNode41.Name = "sound"
        TreeNode41.Text = "Son système"
        TreeNode42.Name = "screenshot"
        TreeNode42.Text = "Capture d'écran"
        TreeNode43.Name = "screenshotpaint"
        TreeNode43.Text = "Capture d'écran + édition"
        TreeNode44.Name = "kill"
        TreeNode44.Text = "Terminer processus"
        TreeNode45.Name = "others"
        TreeNode45.Text = "Autres"
        Me.treeActions.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode12, TreeNode21, TreeNode28, TreeNode35, TreeNode41, TreeNode45})
        Me.treeActions.Size = New System.Drawing.Size(295, 417)
        Me.treeActions.TabIndex = 0
        '
        'listTouches
        '
        Me.listTouches.FormattingEnabled = True
        Me.listTouches.Location = New System.Drawing.Point(313, 32)
        Me.listTouches.Name = "listTouches"
        Me.listTouches.Size = New System.Drawing.Size(128, 95)
        Me.listTouches.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Sélectionnez une action :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(310, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Sélectionnez une touche :"
        '
        'bCapturer
        '
        Me.bCapturer.Location = New System.Drawing.Point(313, 133)
        Me.bCapturer.Name = "bCapturer"
        Me.bCapturer.Size = New System.Drawing.Size(128, 23)
        Me.bCapturer.TabIndex = 3
        Me.bCapturer.Text = "Capturer"
        Me.ToolTipInfo.SetToolTip(Me.bCapturer, "La fonction de capture est désactivée car vous n'êtes pas connecté." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Rendez-vous " & _
        "dans la fenêtre principale pour ce faire." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.bCapturer.UseVisualStyleBackColor = True
        '
        'bSupprimer
        '
        Me.bSupprimer.Location = New System.Drawing.Point(313, 162)
        Me.bSupprimer.Name = "bSupprimer"
        Me.bSupprimer.Size = New System.Drawing.Size(128, 23)
        Me.bSupprimer.TabIndex = 3
        Me.bSupprimer.Text = "Supprimer"
        Me.bSupprimer.UseVisualStyleBackColor = True
        '
        'bValider
        '
        Me.bValider.Enabled = False
        Me.bValider.Location = New System.Drawing.Point(313, 426)
        Me.bValider.Name = "bValider"
        Me.bValider.Size = New System.Drawing.Size(128, 23)
        Me.bValider.TabIndex = 3
        Me.bValider.Text = "Valider"
        Me.bValider.UseVisualStyleBackColor = True
        '
        'personnalise
        '
        Me.personnalise.Enabled = False
        Me.personnalise.Location = New System.Drawing.Point(12, 458)
        Me.personnalise.Name = "personnalise"
        Me.personnalise.Size = New System.Drawing.Size(295, 20)
        Me.personnalise.TabIndex = 4
        '
        'bParcourir
        '
        Me.bParcourir.Enabled = False
        Me.bParcourir.Location = New System.Drawing.Point(313, 456)
        Me.bParcourir.Name = "bParcourir"
        Me.bParcourir.Size = New System.Drawing.Size(128, 23)
        Me.bParcourir.TabIndex = 3
        Me.bParcourir.Text = "Parcourir"
        Me.bParcourir.UseVisualStyleBackColor = True
        '
        'SelectionnerCustom
        '
        Me.SelectionnerCustom.FileName = "OpenFileDialog1"
        '
        'AjouterLigne
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 491)
        Me.Controls.Add(Me.personnalise)
        Me.Controls.Add(Me.bParcourir)
        Me.Controls.Add(Me.bValider)
        Me.Controls.Add(Me.bSupprimer)
        Me.Controls.Add(Me.bCapturer)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.listTouches)
        Me.Controls.Add(Me.treeActions)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "AjouterLigne"
        Me.Text = "Ajouter"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents treeActions As System.Windows.Forms.TreeView
    Friend WithEvents listTouches As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bCapturer As System.Windows.Forms.Button
    Friend WithEvents bSupprimer As System.Windows.Forms.Button
    Friend WithEvents bValider As System.Windows.Forms.Button
    Friend WithEvents personnalise As System.Windows.Forms.TextBox
    Friend WithEvents bParcourir As System.Windows.Forms.Button
    Friend WithEvents ToolTipInfo As System.Windows.Forms.ToolTip
    Friend WithEvents SelectionnerCustom As System.Windows.Forms.OpenFileDialog
End Class
