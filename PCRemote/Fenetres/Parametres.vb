﻿Public Class Parametres

    Private Sub cbDemarrage_CheckedChanged(sender As Object, e As EventArgs) Handles cbDemarrage.CheckedChanged
        If cbDemarrage.Checked Then
            AddStartup("PC Remote", Application.ExecutablePath & " -notification")
        Else
            RemoveStartup("PC Remote")
        End If
    End Sub

    Private Sub Parametres_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.Icone_V2
        cbDemarrage.Checked = MainParametersProvider.Demarrage
        cbModeGraphique.Checked = MainParametersProvider.GraphicMode
        cbDisplayLog.Checked = MainParametersProvider.DisplayLog
        tbFolderScreenshots.Text = MainParametersProvider.ScreenshotsFolder
        tbNomOrdinateur.Text = MainParametersProvider.NameToDisplay
    End Sub

    Private Sub bSave_Click(sender As Object, e As EventArgs) Handles bSave.Click
        If tbNomOrdinateur.Text = "" Or tbNomOrdinateur.Text = "{MACHINENAME}" Then 'Permet de remplacer des alias par leurs valeurs... Pas très utile non
            tbNomOrdinateur.Text = Environment.MachineName
        ElseIf tbNomOrdinateur.Text = "{USERNAME}" Then
            tbNomOrdinateur.Text = Environment.UserName
        End If
        MainParametersProvider.ChangeParameter("ComputerName", tbNomOrdinateur.Text)
        MainParametersProvider.NameToDisplay = tbNomOrdinateur.Text
        MainParametersProvider.changeparameter("ScreenshotsFolder", tbFolderScreenshots.Text)
        MainParametersProvider.ScreenshotsFolder = tbFolderScreenshots.Text
        LoadAllParameters()
        If Principal.tryAutoDisconnect Then Principal.tryAutoConnect() 'Pour afficher de nouveau le bon nom
        Me.Close()
    End Sub

    Private Sub tbNomOrdinateur_KeyDown(sender As Object, e As KeyPressEventArgs) Handles tbNomOrdinateur.KeyPress 'Evite que l'utilisateur entre un nom de plus de seize caractères
        If tbNomOrdinateur.Text.Length = 16 And e.KeyChar <> vbBack Then
            e.KeyChar = ""
            toolTipAide.Show("L'écran standard pour Arduino a une largeur de seize caractères." & vbCrLf & "Votre nom ne peut donc pas dépasser cette longueur.", tbNomOrdinateur)
        End If
    End Sub

    Private Sub tbFolderScreenshots_TextChanged(sender As Object, e As EventArgs) Handles tbFolderScreenshots.TextChanged, tbNomOrdinateur.TextChanged
        ActiverDesactiverBoutonEnregistrer()
    End Sub

    Private Sub bBrowseFolderScreenshots_Click(sender As Object, e As EventArgs) Handles bBrowseFolderScreenshots.Click
        With ChoisirDossier
            .Description = "Choissez le dossier de destination des captures d'écran."
            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = tbFolderScreenshots.Text
            .ShowNewFolderButton = True
            .ShowDialog()
        End With
        tbFolderScreenshots.Text = ChoisirDossier.SelectedPath
    End Sub

    Private Sub ActiverDesactiverBoutonEnregistrer() 'Active ou désactive le bouton d'enregistrement en fonction des paramètres entrés
        While tbFolderScreenshots.Text.Contains("/")
            tbFolderScreenshots.Text = tbFolderScreenshots.Text.Replace("/", "\")
        End While
        While tbFolderScreenshots.Text.Contains("\\")
            tbFolderScreenshots.Text = tbFolderScreenshots.Text.Replace("\\", "\")
        End While
        If tbFolderScreenshots.Text = "" Then
            bSave.Enabled = False
            Me.Text = "Paramètres"
        ElseIf My.Computer.FileSystem.DirectoryExists(tbFolderScreenshots.Text) = False Then
            bSave.Enabled = False
            Me.Text = "Paramètres (dossier introuvable)"
            'ElseIf tbNomOrdinateur.Text = "" Then
            '    bSave.Enabled = False
            '    Me.Text = "Paramètres (champ vide)"
        Else
            bSave.Enabled = True
            Me.Text = "Paramètres"
        End If
    End Sub

    Private Sub ModeConfiguration_CheckedChanged(sender As Object, e As EventArgs) Handles cbModeGraphique.CheckedChanged
        If sender.checked = MainParametersProvider.GraphicMode Then Exit Sub
        MainParametersProvider.ChangeParameter("GraphicMode", sender.checked)
        MainParametersProvider.GraphicMode = Not MainParametersProvider.GraphicMode
        Principal.UpdateLayout()
    End Sub

    Private Sub cbDisplayLog_CheckedChanged(sender As Object, e As EventArgs) Handles cbDisplayLog.CheckedChanged
        If sender.checked = MainParametersProvider.DisplayLog Then Exit Sub
            MainParametersProvider.ChangeParameter("DisplayLog", sender.checked)
        MainParametersProvider.DisplayLog = Not MainParametersProvider.DisplayLog
        Principal.UpdateLayout()
    End Sub
End Class