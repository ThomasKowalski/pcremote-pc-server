﻿Imports System.IO.Ports
Imports System.Text
Imports System.Globalization

Public Class Principal
    Dim ListID As New List(Of String)
    Dim ListNoms As New Dictionary(Of String, String)
    Dim ListActions As New Dictionary(Of String, String) 'Déclaration de la liste des actions (Play, Pause...) du fichier de configuration. Il est pour l'instant vide.
    Dim ListSignaux As New Dictionary(Of String, String) 'Déclaration de la liste des signaux (de la télécommande, sous forme de chiffres) du fichier de configuration. Il est pour l'instant vide.
    'Déclaration de la liste des types d'actions (iTunes, CMD...) du fichier de configuration. Il est pour l'instant vide.
    Dim CurrentActions As New List(Of String)

    Dim TempString As String = "" 'Variable qui sera plus tard utilisée pour stocker la réception COM

    Dim WaitingCapture As Boolean = False 'Variable précisant si oui ou non la valeur qui sera reçue doit être interprétée ou non (non interprétée = ajoutée au fichier de configuration).
    Dim CaptureGraphique As Boolean = False 'Mode de capture graphique en cours

    Dim Connecte As Boolean = False 'Est ce qu'on est connecté à l'Arduino

    Delegate Sub myMethodDelegate(ByVal [text] As String)
    Dim myDelegate As New myMethodDelegate(AddressOf ShowString)


    Private Function TryCreateiTunesObject()
        For Each Process In Diagnostics.Process.GetProcesses()
            If Process.ProcessName.ToLower = "itunes" Then
                iTunesObject = New iTunesControler()
                Return True
                Exit Function
            End If
        Next
        iTunesObject = Nothing
        Return False
    End Function

    Sub ShowString(ByVal receivedString As String)
        If timerConfirmConnect.Enabled = True Then
            timerConfirmConnect.Enabled = False
        End If
        If TimerCoDeco.Enabled Then Exit Sub 'Oui ça en fait c'est un timer qui est là parce que quand j'envoie un message à l'Arduino ce qui a pour effet de foutre la merde un petit peu. Du coup j'attends 100ms (je crois) avant de prendre de nouveau en compte les données.
        If TimerReception.Enabled = True Then 'Pareil qu'au dessus, j'attends un certain temps entre chaque réception, pour éviter de faire trop d'actions à la fois à cause d'un bouton laissé appuyé un poil trop longtemps
            TempString = ""
            Exit Sub
        End If
        TempString &= receivedString 'Dans le cas où on accepte les données, on ajoute la dernière chaîne reçue à celle d'avant. Parce que tel un bad boy je reçois les signaux par morceaux.
        If receivedString.Contains(vbCrLf) Then
            receivedString = TempString.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)(0) 'Donc là ça veut dire qu'on a un signal en entier donc
            TempString = "" 'On reset la chaine temporaire
            TimerReception.Enabled = True 'Et on empêche de recevoir pendant un certain temps
        Else
            Exit Sub 'Et sinon ça veut dire qu'on a pas encore toute la chaîne donc on attend la suite
        End If
        If receivedString = "pong" Then
            TimerPresence.Enabled = False
            TimerPresence.Enabled = True
            TimerPresenceReception.Enabled = False
            tLog.Text += "[" & Heure() & "] " & MarqueurInfos & " L'Arduino est bien connecté !" & vbCrLf
            Exit Sub
        End If
        If CaptureGraphique Then 'Ca c'est si on a l'UI de création de contrôle, on envoie le signal dans la listbox et on l'interprète pas
            If AjouterLigne.listTouches.Items.IndexOf(receivedString) = -1 Then AjouterLigne.listTouches.Items.Add(receivedString)
            AjouterLigne.boutonValider()
            tLog.Text += "[" & Heure() & "] [Réception COM] Signal capturé (interface graphique) : " & receivedString & vbCrLf
            Exit Sub
        ElseIf WaitingCapture Then 'Pareil qu'avant, sauf que là le signal on l'ajoute au fichier de config sur la droite de la fenêtre
            If MainParametersProvider.GraphicMode Then
                If Not ListBoxTriggers.Items.Contains(receivedString) And ListBoxDescription.SelectedItem <> Nothing Then ListBoxTriggers.Items.Add(receivedString)
            Else
                If tConfig.Text = "" Then
                    tConfig.Text &= receivedString & ";"
                Else
                    tConfig.Text &= vbCrLf & receivedString & ";"
                End If
                WaitingCapture = False
            End If
            tLog.Text += "[" & Heure() & "] [Réception COM] Signal capturé : " & receivedString & vbCrLf
            Exit Sub
        End If

        If receivedString = "" Then Exit Sub

        Dim ID As String = ""
        For Each Signal In ListSignaux 'Là, on analyse chaque signal de la liste et on regarde si il correspond à celui reçu. Ca peut paraître compliqué, la ligne du dessous, mais dites vous que c'est juste pour éviter de faire un Split(",") et un For-ception
            If (Signal.Value.StartsWith(receivedString & ",")) Or (Signal.Value.ToString.Contains("," & receivedString & ",")) Or (Signal.Value.ToString.EndsWith("," & receivedString)) Or (Signal.Value.ToString.Split(",").Length = 1 And Signal.Value = receivedString) Then
                ID = Signal.Key
                Exit For
            End If
        Next
        If ID = "" Then 'Dans ce cas là, le signal est pas dans la liste
            tLog.Text += "[" & Heure() & "] [Réception COM] Signal non reconnu : " & receivedString & vbCrLf
            SendSerialData("Signal", "non reconnu", False)
            PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, "Signal non reconnu"
                                             ) 'Et on actualise le panneau de visualisation
        Else 'Sinon on va s'amuser à regarder à quoi il correspond
            Dim Interprete = InterpreteData(ID)
            If Interprete <> "" Then
                tLog.Text += "[" & Heure() & "] [Réception COM] Signal reconnu : " & receivedString & " | Action : [" & Interprete & "]" & vbCrLf
                SendSerialData("Reconnu", Interprete, False) 'On envoie à l'Arduino l'ordre d'afficher l'action et le type d'action
                PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, Interprete) 'Et on actualise le panneau de visualisation
            Else
                tLog.Text += "[" & Heure() & "] [Réception COM] Signal reconnu : " & receivedString & " | Aucune action assignée." & vbCrLf
                SendSerialData("Non", "assigne", False) 'On envoie à l'Arduino l'ordre d'afficher l'action et le type d'action
                PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, "Non assigné") 'Et on actualise le panneau de visualisation
            End If
        End If
    End Sub

    Private Function centrer(chaine As String, largeur As Integer, alignToRight As Boolean) As String 'Cette fonction nous permet de centrer la chaine sur l'écran en fonction de sa longueur, de la largeur de l'écran (en caractères) et d'un troisième paramètre alignToRight. C'est relativement basique, on insère un certain nombre d'espace et on renvoie la chaîne modifiée.
        Dim espaceRestants As Integer = largeur - chaine.Length
        If espaceRestants Mod 2 = 0 Then
            For i = 0 To espaceRestants / 2 - 1
                chaine = " " & chaine
            Next
        Else
            If alignToRight Then
                Return centrer(" " & chaine, largeur, True)
            Else
                Return centrer(chaine & " ", largeur, False)
            End If
        End If
        Return chaine
    End Function

    Private Sub PCRemote_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        tLog.Text += "[" & Heure() & "] " & MarqueurCore & " Démarrage de PC Remote. (Version : " & VersionPCRemote & ")" & vbCrLf
        tLog.Text += "[" & Heure() & "] " & MarqueurCore & " Lancement au démarrage de Windows : " & LancementDemarrage() & vbCrLf
        tLog.Text += "[" & Heure() & "] " & MarqueurDependances & " Copie des dépendances temporaires..." & vbCrLf
        tLog.Text += "[" & Heure() & "] " & MarqueurDependances & " Copie terminée." & vbCrLf
        Dim BaudRates() As String = {"300", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200"}
        cmbBaud.Items.AddRange(BaudRates)
        cmbBaud.SelectedIndex = 4

        MainParametersProvider = New ParametersManager()
        WizmoInstance = New Wizmo()
        NirCMDInstance = New NirCMD()
        MainLogProvider = New LogProvider()
        MainConfigurationParser = New ConfigurationParser()
        COMManager = New COM()

        Try
            For Each Port In COMManager.GetNames()
                cmbPort.Items.Add(Port)
            Next
            cmbPort.SelectedIndex = 0
        Catch
            tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Aucun port COM connecté." & vbCrLf
        End Try

        LoadAllParameters()
        tryAutoConnect()
        PanelVisualisation.Show()
        PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, "Aucun")
        ChargerConfig()
        If ApplyConfig() Then PopulateLists()
        UpdateLayout()

        Dim CommandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs
        For i As Integer = 0 To CommandLineArgs.Count - 1
            If CommandLineArgs(i) = "-notification" Then
                Me.Height = 0
                Me.Width = 0
                timerNotif.Enabled = True
            Else
                tLog.Text += "[" & Heure() & "] " & MarqueurCore & " Argument non reconnu : " & CommandLineArgs(i) & vbCrLf
            End If
        Next
        If (ListBoxDescription.Items.Count > 0) Then
            ListBoxDescription.SelectedIndex = 0
            bAjouter.Enabled = Not ListBoxDescription.SelectedItem = Nothing
            bCapturer.Enabled = Not ListBoxDescription.SelectedItem = Nothing
            PopulateTriggersAndActions()
        End If
    End Sub

    Private Sub SerialPort_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles sp.DataReceived
        Dim str As String = sp.ReadExisting()
        Invoke(myDelegate, str)
    End Sub

    Private Sub bCapturer_Click(sender As Object, e As EventArgs) Handles bCapturer.Click
        CapturerGraphique()
    End Sub

    Private Sub bEnregistrer_Click(sender As Object, e As EventArgs) Handles bEnregistrer.Click
        If tConfig.Text = "" Then
            Dim confirmNullConfig = MessageBox.Show("Vous n'avez entré aucune configuration. Voulez-vous vraiment enregistrer une configuration vide ?", "Configuration vide", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
            If confirmNullConfig = vbNo Then
                Exit Sub
            End If
        End If
        Dim saveFile As New IO.StreamWriter(Environ("APPDATA") & "\PCRemote\" & "\config.cfg", False)
        saveFile.Write(tConfig.Text)
        saveFile.Close()
        lblConfigModifiee.Visible = False
        ApplyConfig()
    End Sub

    Private Sub bAjouter_Click(sender As Object, e As EventArgs) Handles bAjouter.Click
        If MainParametersProvider.GraphicMode Then
            If (bAjouter.Text = "Ajouter") Then
                If ListBoxDescription.SelectedItem = Nothing Then
                    MessageBox.Show("Veuillez d'abord sélectionner un évènement auquel ajouter une action.", "Impossible d'ajouter une action.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                CurrentActions = New List(Of String)
                For Each Action In ListBoxActions.Items
                    CurrentActions.Add(Action)
                Next
                Dim PossibleActions As Dictionary(Of String, String) = MainConfigurationParser.GoodTranslationDictionnary
                ListBoxActions.Items.Clear()
                For Each Action In PossibleActions
                    If CurrentActions.Contains(Action.Value) = False Then ListBoxActions.Items.Add(Action.Value)
                Next
                bAjouter.Text = "Valider"
                ' bAjouter.Enabled = False
            Else
                For Each Action In ListBoxActions.SelectedItems
                    CurrentActions.Add(Action)
                Next
                ListBoxActions.Items.Clear()
                For Each Action In CurrentActions
                    'If Not ListBoxActions.Items.Contains(Action) Then
                    ListBoxActions.Items.Add(Action)
                    ' End If
                Next
                Dim NewActionsValue As String = ""
                For Each Action In ListBoxActions.Items
                    NewActionsValue &= MainConfigurationParser.getCommandFromTranslation(Action) & "|"
                Next
                NewActionsValue = NewActionsValue.Substring(0, NewActionsValue.Length - 1)
                ListActions(ListID(ListBoxDescription.SelectedIndex)) = NewActionsValue
                WriteFileFromLists()
                bAjouter.Text = "Ajouter"
            End If
        Else
            If Me.Connected = False Then
                MessageBox.Show("Vous devez vous connecter à l'Arduino avant de pouvoir ajouter des lignes automatiquement.", "Aucune connexion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                AjouterLigne.Show()
            End If
        End If
    End Sub
#Region "Divers"
    Private Sub compilerConfig_Click(sender As Object, e As EventArgs) Handles bCompiler.Click
        tConfig.Text = MainConfigurationParser.Normalize(tConfig.Text)
        Verify()
    End Sub
    Private Sub Principal_GotFocus(sender As Object, e As EventArgs) Handles Me.GotFocus
        UpdateLayout()
    End Sub
    Private Sub btnConnect_Click(sender As System.Object, e As System.EventArgs) Handles bConnecter.Click
        If cmbBaud.Text = "" Or cmbPort.Text = "" Then
            Exit Sub
        End If
        connectCOM(False)
    End Sub

    Private Sub btnDisconnect_Click(sender As System.Object, e As System.EventArgs) Handles bDeconnecter.Click
        timerConfirmConnect.Enabled = False
        disconnectCOM(False)
    End Sub

    Private Sub Principal_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Hide()
        e.Cancel = True
        Exit Sub
    End Sub
    Private Sub ContextMenuDescriptions_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuDescriptions.Opening
        SupprimerToolStripMenuItemDescription.Enabled = ListBoxDescription.SelectedItems.Count > 0
        RenommerToolStripMenuItem.Visible = ListBoxDescription.SelectedItems.Count = 1
    End Sub
    Private Sub ContextMenuActions_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuActions.Opening
        SupprimerToolStripMenuItemActions.Enabled = ListBoxActions.SelectedItems.Count > 0
    End Sub

    Private Sub ContextMenuTriggers_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuTriggers.Opening
        SupprimerToolStripMenuItemTriggers.Enabled = ListBoxTriggers.SelectedItems.Count > 0
    End Sub
    Private Sub lblPort_Click(sender As Object, e As EventArgs) Handles lblPort.Click, lblNoCOMDetected.Click
        For Each Port In COMManager.GetNames()
            cmbPort.Items.Add(Port)
        Next
        UpdateLayout()
    End Sub
    Private Sub txtIn_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tLog.KeyPress
        e.KeyChar = "" 'Evite que l'utilisateur puisse taper dans la zone de réception COM. Ce serait con de faire ça mais on sait jamais.
    End Sub
    Private Sub tConfig_TextChanged(sender As Object, e As EventArgs) Handles tConfig.TextChanged
        lblConfigModifiee.Visible = True
    End Sub
    Private Sub txtIn_TextChanged(sender As Object, e As EventArgs) Handles tLog.TextChanged
        tLog.Select(tLog.TextLength, 0)
        tLog.ScrollToCaret()
    End Sub
    Private Sub bClearLog_Click(sender As Object, e As EventArgs) Handles bClearLog.Click
        MainLogProvider.WriteLog(tLog.Text)
        tLog.Text = ""
    End Sub

    Private Sub bParametres_Click(sender As Object, e As EventArgs) Handles bParametres.Click
        Parametres.ShowDialog()
    End Sub

    Private Sub bAide_Click(sender As Object, e As EventArgs) Handles bAide.Click
        Process.Start("http://thomaskowalski.net/pcremote")
    End Sub
#End Region
#Region "Timers"
    Private Sub TimerReception_Tick(sender As Object, e As EventArgs) Handles TimerReception.Tick
        TimerReception.Enabled = False
    End Sub
    Private Sub TimerCoDeco_Tick(sender As Object, e As EventArgs) Handles TimerCoDeco.Tick
        bConnecter.Enabled = True
        bDeconnecter.Enabled = True
        TimerCoDeco.Enabled = False
    End Sub
    Private Sub TimerPresence_Tick(sender As Object, e As EventArgs) Handles TimerPresence.Tick
        Try
            sp.WriteLine("3ping" & vbCr)
            tLog.Text += "[" & Heure() & "] " & MarqueurInfos & " Vérification que l'Arduino est connecté..." & vbCrLf
            TimerPresenceReception.Enabled = True
        Catch ex As Exception
            'MessageBox.Show("Erreur de connexion.")
        End Try
    End Sub

    Private Sub TimerPresenceReception_Tick(sender As Object, e As EventArgs) Handles TimerPresenceReception.Tick
        'txtIn.Text += "[" & Heure() & "] " & MarqueurInfos & " Aucune réponse de l'Arduino. Déconnexion." & vbCrLf
        'disconnectCOM(True)
        'TimerPresence.Enabled = False
        Debug.Print("pas de réponse reçue")
        TimerPresenceReception.Enabled = False
    End Sub
    Private Sub timerNotif_Tick(sender As Object, e As EventArgs) Handles timerNotif.Tick
        'On ne peut pas cacher la fenêtre dans le Load() donc on fait un timer... Impossible de faire autrement.
        Me.Width = 1026
        If MainParametersProvider.DisplayLog Then Me.Height = 500 Else Me.Height = 536
        Me.Hide()
        timerNotif.Enabled = False
    End Sub

    Private Sub timerConfirmConnect_Tick(sender As Object, e As EventArgs) Handles timerConfirmConnect.Tick
        timerConfirmConnect.Enabled = False
        MessageBox.Show("PCRemote Server s'est connecté au port, mais il n'a reçu aucun signal de confirmation de connexion de l'Arduino. Merci de vous reconnecter ou de changer de port.", "Aucune réponse reçue.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        disconnectCOM(False)
    End Sub
#End Region

    Private Function Verify() As Boolean
        Dim ParsingResult As Object() = MainConfigurationParser.Parse(tConfig.Text)
        Select Case ParsingResult(0)
            Case ConfigurationParser.ParseError.BadAction
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : action non reconnue."
            Case ConfigurationParser.ParseError.BadCommaNumber
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : nombre illégal de points-virgules."
            Case ConfigurationParser.ParseError.BadKeyDeclaration
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : une touche est déclarée par un caractère alphabétique."
            Case ConfigurationParser.ParseError.GenericError
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : erreur lors de l'analyse (manque t-il un élément ?)"
            Case ConfigurationParser.ParseError.KeyAlreadyUsed
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : une touche déjà utilisée est déclarée."
            Case ConfigurationParser.ParseError.IDAlreadyUsed
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : un ID déjà utilisé est déclaré."
            Case ConfigurationParser.ParseError.NoCategory
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : aucune catégorie d'action n'est déclarée."
            Case ConfigurationParser.ParseError.NoConfiguration
                lblParseError.Text = "[Remarque] Pas de configuration entrée."
            Case ConfigurationParser.ParseError.NoKey
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : aucune touche n'est déclarée."
            Case ConfigurationParser.ParseError.NoParameter
                lblParseError.Text = "[Erreur] Ligne " & ParsingResult(1) & " : aucun paramètre ou action n'est déclaré."
            Case ConfigurationParser.ParseError.Success
                lblParseError.Text = "[Succès] Configuration correcte !"
                Return True
        End Select
        If Not MainParametersProvider.GraphicMode Then Return False Else Return True
    End Function

    Private Sub bNouveau_Click(sender As Object, e As EventArgs) Handles bNouveau.Click
        Dim NouvelElement As String = InputBox("Entrez un nom pour votre nouvelle action.", "Nouvelle action")
        If NouvelElement = "" Then Exit Sub
        Dim NewID As Integer = ListID.Count + 1
        ListID.Add(NewID)
        ListNoms.Add(NewID, NouvelElement)
        ListSignaux.Add(NewID, "")
        ListActions.Add(NewID, "")
        ListBoxDescription.Items.Add(NouvelElement)
        ListBoxDescription.SelectedIndex = ListBoxDescription.Items.Count - 1
        PopulateTriggersAndActions()
        WriteFileFromLists()
    End Sub
#Region "Listes"
    Private Sub ListBoxDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxDescription.SelectedIndexChanged
        bAjouter.Enabled = Not ListBoxDescription.SelectedItem = Nothing
        bCapturer.Enabled = Not ListBoxDescription.SelectedItem = Nothing
        PopulateTriggersAndActions()
    End Sub

    Private Sub ListBoxActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxActions.SelectedIndexChanged
        If bAjouter.Text = "Valider" And ListBoxActions.SelectedItem <> Nothing Then
            CurrentActions.Add(ListBoxActions.SelectedItem)
            ListBoxActions.Items.Remove(ListBoxActions.SelectedItem)
        End If
    End Sub
#End Region
#Region "COM"
    Public Function tryAutoDisconnect()
        Return disconnectCOM(True)
    End Function
    Public Sub tryAutoConnect()
        connectCOM(True)
    End Sub

    Private Sub connectCOM(auto As Boolean)
        Try
            Dim PortNumber = cmbPort.SelectedItem.ToString
            PortNumber = PortNumber.Substring(PortNumber.IndexOf("(") + 1, PortNumber.Length - PortNumber.IndexOf("(") - 2) 'Transformation de "Arduino [...] (COMX)" en "COMX"
            sp.BaudRate = cmbBaud.SelectedItem.ToString
            sp.PortName = PortNumber
            sp.Open()
            If sp.IsOpen Then
                cmbPort.Enabled = False
                cmbBaud.Enabled = False
                tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Connecté (Port : " & cmbPort.Text & " / Baud : " & cmbBaud.Text & ")" & vbCrLf
                Connecte = True
                Me.Text = "PCRemote (Connecté)"
                PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, "Aucun")
                SendSerialData("Connecte", MainParametersProvider.NameToDisplay, False)
                AjouterLigne.captureButton()
                bConnecter.Enabled = False
                TimerCoDeco.Enabled = True
                timerConfirmConnect.Enabled = True
                TimerPresence.Enabled = True
                UpdateLayout()
            End If
        Catch
            If auto Then
                tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Impossible de se connecter automatiquement. (Port : " & cmbPort.Text & " / Baud : " & cmbBaud.Text & ")" & vbCrLf
            Else
                tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Impossible de se connecter. (Port : " & cmbPort.Text & " / Baud : " & cmbBaud.Text & ")" & vbCrLf
            End If
            sp.Close()
        End Try
    End Sub
    Private Function disconnectCOM(auto As Boolean)
        Try
            SendSerialData("Deconnecte", "Att. connexion", True)
            sp.Close()
            cmbPort.Enabled = True
            cmbBaud.Enabled = True
            tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Déconnecté." & vbCrLf
            Me.Text = "PCRemote"
            Connecte = False
            AjouterLigne.captureButton()
            PanelVisualisation.ModifierInfos(Me.Connected, cmbPort.Text, cmbBaud.Text, "Aucun")
            TimerCoDeco.Enabled = True
            UpdateLayout()
            Return True
        Catch
            If auto Then
                tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Impossible de fermer automatiquement la connexion." & vbCrLf
            Else
                tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Impossible de fermer la connexion." & vbCrLf
            End If
            Return False
        End Try
    End Function
    Public WithEvents sp As New SerialPort

    Public Shared Function RemoveDiacritics(s As [String]) As [String]
        Dim normalizedString As [String] = s.Normalize(NormalizationForm.FormD)
        Dim stringBuilder As New StringBuilder()

        For i As Integer = 0 To normalizedString.Length - 1
            Dim c As [Char] = normalizedString(i)
            If CharUnicodeInfo.GetUnicodeCategory(c) <> UnicodeCategory.NonSpacingMark Then
                stringBuilder.Append(c)
            End If
        Next

        Return stringBuilder.ToString().Normalize(NormalizationForm.FormC)
    End Function

    Public Sub SendSerialData(ByVal typeAction As String, action As String, autoConnect As Boolean)
        typeAction = RemoveDiacritics(typeAction)
        action = RemoveDiacritics(action)
        If Connected() Then
            sp.WriteLine("1" & centrer(typeAction, 16, False) & vbCr)
            sp.WriteLine("2" & centrer(action, 16, False) & vbCr)
        ElseIf autoConnect Then
            Using com1 As IO.Ports.SerialPort = My.Computer.Ports.OpenSerialPort(cmbPort.Text)
                com1.WriteLine("1" & centrer(typeAction, 16, False) & vbCr)
                com1.WriteLine("2" & centrer(action, 16, False) & vbCr)
            End Using
        Else
            tLog.Text += "[" & Heure() & "] " & MarqueurInfos & " Impossible d'envoyer le message à l'Arduino car le port est fermé." & vbCrLf
        End If
    End Sub
#End Region

#Region "Context Menus"
#Region "Description"
    Private Sub RenommerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenommerToolStripMenuItem.Click
        Dim Input = InputBox("Entrez le nouveau nom de l'événement.", "Renommage", ListBoxDescription.SelectedItem)
        If Input = "" Then Exit Sub
        ListNoms(ListID(ListBoxDescription.SelectedIndex)) = Input
        WriteFileFromLists()
        Dim SelectedItem As Integer = ListBoxDescription.SelectedIndex
        PopulateLists()
        ListBoxDescription.SelectedIndex = SelectedItem
        bAjouter.Enabled = Not ListBoxDescription.SelectedItem = Nothing
        bCapturer.Enabled = Not ListBoxDescription.SelectedItem = Nothing
        PopulateTriggersAndActions()
    End Sub
    Private Sub ToolStripMenuItemDescription_Click(sender As Object, e As EventArgs) Handles SupprimerToolStripMenuItemDescription.Click
        SupprimerSelectionnesDescription()
    End Sub
    Private Sub ListBoxDescription_KeyDown(sender As Object, e As KeyEventArgs) Handles ListBoxDescription.KeyDown
        If e.KeyCode = Keys.Delete Then
            SupprimerSelectionnesDescription()
        End If
    End Sub
    Private Sub SupprimerSelectionnesDescription()
        Dim TempList As New List(Of String)
        Dim Resultat As DialogResult
        If ListBoxActions.SelectedItems.Count > 1 Then
            Resultat = MessageBox.Show("Etes vous sûr de vouloir supprimer les évènements sélectionnés ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        Else
            Resultat = MessageBox.Show("Etes vous sûr de vouloir supprimer l'évènement sélectionné ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        End If
        If Resultat = Windows.Forms.DialogResult.No Then Exit Sub
        For Each Evenement In ListBoxDescription.SelectedItems
            TempList.Add(Evenement)
        Next
        For Each Evenement In TempList
            ListBoxDescription.Items.Remove(Evenement)
        Next
        Dim ListStillHereID As New List(Of String)
        For Each ID In ListID
            If ListBoxDescription.Items.IndexOf(ListNoms(ID)) <> -1 Then
                ListStillHereID.Add(ID)
            Else
                ListNoms.Remove(ID)
                ListActions.Remove(ID)
                ListSignaux.Remove(ID)
            End If
        Next
        ListID.Clear()
        For Each ID In ListStillHereID
            ListID.Add(ID)
        Next
        If ListBoxDescription.Items.Count > 0 Then
            ListBoxDescription.SelectedIndex = 0
        Else
            ListBoxTriggers.Items.Clear()
            ListBoxActions.Items.Clear()
        End If
        WriteFileFromLists()
        PopulateTriggersAndActions()

    End Sub

#End Region
#Region "Triggers"
    Private Sub CapturerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CapturerToolStripMenuItem.Click
        CapturerGraphique()
    End Sub

    Private Sub ManuellementToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManuellementToolStripMenuItem.Click
        Dim Input As String = InputBox("Entrez le signal à ajouter ci-dessous.", "Ajouter un signal")
        If Input = "" Then Exit Sub
        Dim NewSignal As Integer

        If Integer.TryParse(Input, NewSignal) Then
            If ListBoxTriggers.Items.Contains(Input) = False Then
                ListBoxTriggers.Items.Add(NewSignal)
                Dim NewTriggerValue As String = ""
                For Each Trigger In ListBoxTriggers.Items
                    NewTriggerValue &= Trigger & ","
                Next
                NewTriggerValue = NewTriggerValue.Substring(0, NewTriggerValue.Length - 1)
                ListSignaux(ListID(ListBoxDescription.SelectedIndex)) = NewTriggerValue
                WriteFileFromLists()
                PopulateTriggersAndActions()
            End If
        Else
            MessageBox.Show("Vous devez entrer un nombre entier (x ∈ ℤ)", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub ToolStripMenuItemTriggers_Click(sender As Object, e As EventArgs) Handles SupprimerToolStripMenuItemTriggers.Click
        SupprimerSelectionnesTriggers()
    End Sub

    Private Sub ListBoxTriggers_KeyDown(sender As Object, e As KeyEventArgs) Handles ListBoxTriggers.KeyDown
        If e.KeyCode = Keys.Delete Then SupprimerSelectionnesTriggers()
    End Sub
    Private Sub SupprimerSelectionnesTriggers()
        Dim TempList As New List(Of String)
        For Each Trigger In ListBoxTriggers.SelectedItems
            TempList.Add(Trigger)
        Next
        For Each Trigger In TempList
            ListBoxTriggers.Items.Remove(Trigger)
        Next
        Dim NewTriggersValue As String = ""
        For Each Trigger In ListBoxTriggers.Items
            NewTriggersValue &= Trigger & ","
        Next
        If NewTriggersValue <> "" Then
            ListSignaux(ListID(ListBoxDescription.SelectedIndex)) = NewTriggersValue.Substring(0, NewTriggersValue.Length - 1)
        Else
            ListSignaux(ListID(ListBoxDescription.SelectedIndex)) = NewTriggersValue
        End If
        WriteFileFromLists()
    End Sub
#End Region
#Region "Actions"
    Private Sub SupprimerToolStripMenuItemActions_Click(sender As Object, e As EventArgs) Handles SupprimerToolStripMenuItemActions.Click
        SupprimerSelectionnesActions()
    End Sub
    Private Sub ListBoxActions_KeyDown(sender As Object, e As KeyEventArgs) Handles ListBoxActions.KeyDown
        If e.KeyCode = Keys.Delete Then SupprimerSelectionnesActions()
    End Sub
    Private Sub SupprimerSelectionnesActions()
        Dim TempList As New List(Of String)
        For Each Action In ListBoxActions.SelectedItems
            TempList.Add(Action)
        Next
        For Each Action In TempList
            ListBoxActions.Items.Remove(Action)

        Next
        Dim NewActionsValue As String = ""
        For Each Action In ListBoxActions.Items
            NewActionsValue &= Action & ","
        Next
        If NewActionsValue <> "" Then
            ListActions(ListID(ListBoxDescription.SelectedIndex)) = NewActionsValue.Substring(0, NewActionsValue.Length - 1)
        Else
            ListActions(ListID(ListBoxDescription.SelectedIndex)) = NewActionsValue
        End If
        WriteFileFromLists()
    End Sub
#End Region
#End Region

#Region "Interprétation"
    Private Function InterpreteData(index As String) As String
        If ListActions(index) = "" Then Return False
        For Each ActionCommand In ListActions(index).Split("|")
            Dim TypeAction As String = ActionCommand.ToLower.Split(",")(0)
            Dim Action As String = ActionCommand.ToLower.Split(",")(1)
            Select Case TypeAction 'Selection du type d'action
                Case "cmd", "commande" 'Puis sélection de l'action
                    LastTypeAction = "Commande"
                    LastAction = Action
                    Shell("cmd.exe /C " & Action)
                Case "vlc" 'Oui bah j'ai pas encore trouvé comment faire
                Case "sound", "son"
                    LastTypeAction = "Son"
                    Select Case Action
                        Case "moins", "minus", "diminuer", "baisser", "decrease", "dec", "-", "dim"
                            LastAction = "Diminuer"
                            NirCMDInstance.CallAction("changesysvolume -1000")
                        Case "plus", "augmenter", "increase", "raise", "inc", "aug", "+"
                            LastAction = "Augmenter"
                            NirCMDInstance.CallAction("changesysvolume 1000")
                        Case "mutetoggle", "togglemute", "toggle"
                            NirCMDInstance.CallAction("mutesysvolume 2")
                            LastAction = "Changer muet"
                        Case "mute", "muet", "off"
                            LastAction = "Muet"
                            NirCMDInstance.CallAction("mutesysvolume 1")
                        Case "nomute", "nonmute", "demute", "nonmuet", "pasmuet", "on"
                            LastAction = "Non muet"
                            NirCMDInstance.CallAction("mutesysvolume 0")
                    End Select
                Case "others"
                    Select Case Action
                        Case "screenshot", "capture", "captureecran", "shot", "screenshotpaint", "capturepaint", "captureecranpaint", "shotpaint"
                            LastTypeAction = "Screenshot"
                            LastAction = "Enregistre."
                            If LastSecond = DateTime.Now.Second Then
                                CompteurSeconde += 1
                            Else
                                CompteurSeconde = 1
                                LastSecond = DateTime.Now.Second
                            End If
                            Dim nomScreenshot As String = MainParametersProvider.ScreenshotsFolder & DateHeure() & "_" & CompteurSeconde
                            NirCMDInstance.CallAction("cmdwait 0 savescreenshot """ & nomScreenshot & ".png""")
                            If Action.Contains("paint") Then
                                Shell("mspaint """ & nomScreenshot & ".png""", AppWinStyle.MaximizedFocus)
                                LastTypeAction = "+Paint"
                            End If
                    End Select
                Case "kill", "terminate", "end", "killprocess", "terminer", "fermer"
                    NirCMDInstance.CallAction("killprocess " & Action)
                    LastTypeAction = "Term. process."
                    LastAction = Action
                Case "launch", "lancer", "execute", "start"
                    LastTypeAction = "Lancer"
                    LastAction = Action
                    Process.Start(Action)
                Case "power", "alimentation"
                    LastTypeAction = "Alimentation"
                    Dim add As String = "!"
                    If Not Action.Contains("!") Then
                        add = Nothing
                    End If
                    Select Case Action
                        Case "logoff", "deco", "deconnexion", "deconnecter", "logoff!", "deco!", "deconnexion!", "deconnecter!"
                            WizmoInstance.CallAction("logoff" & add)
                            LastAction = "Deconnexion"
                        Case "hibernate", "hibernation", "veilleprolongee", "prolongee", "hibernate!", "hibernation!", "veilleprolongee!", "prolongee!"
                            WizmoInstance.CallAction("hibernate" & add)
                            LastAction = "Hibernation"
                        Case "verrouiller", "lock", "verrouiller!", "lock!"
                            WizmoInstance.CallAction("lock" & add)
                            LastAction = "Verrouillage"
                        Case "shutdown", "eteindre", "shutdown!", "eteindre!"
                            WizmoInstance.CallAction("shutdown" & add)
                            LastAction = "Extinction"
                        Case "redemarrer", "reboot", "reboot!", "redemarrer!"
                            WizmoInstance.CallAction("reboot" & add)
                            LastAction = "Redémarrage"
                        Case "veille", "standby", "veille!", "standby!"
                            WizmoInstance.CallAction("standby" & add)
                            LastAction = "Veille"
                        Case "screenoff", "monoff", "monitoroff", "eteindreecran", "ecranoff", "screenoff!", "monoff!", "monitoroff!", "eteindreecran!", "ecranoff!"
                            WizmoInstance.CallAction("monoff" & add)
                            LastAction = "Ecran off"
                        Case Else
                            LastAction = "Inconnu"
                            tLog.Text += "[" & Heure() & "] [Power Control] Action non reconnue." & vbCrLf
                    End Select
                Case "itunes"
                    LastTypeAction = "iTunes"
                    If TryCreateiTunesObject() = False Then
                        tLog.Text += "[" & Heure() & "] [iTunes Ctrl] Aucune instance d'iTunes détectée." & vbCrLf
                        LastAction = "iTunes ferme"
                        Return ListNoms(index)
                    End If
                    Select Case Action.ToLower
                        Case "pause", "p"
                            iTunesObject.pauseTrack()
                            LastAction = "Pause"
                        Case "play", "lecture"
                            iTunesObject.playTrack()
                            LastAction = "Play"
                        Case "playpause"
                            iTunesObject.playPause()
                            LastAction = "Play / Pause"
                        Case "stop"
                            iTunesObject.stopTrack()
                            LastAction = "Stop"
                        Case "next", "suivant"
                            iTunesObject.nextTrack()
                            LastAction = "Suivant"
                        Case "prev", "previous", "precedent"
                            iTunesObject.previousTrack()
                            LastAction = "Precedent"
                        Case "moins", "minus", "diminuer", "baisser", "decrease", "dec", "-"
                            iTunesObject.volumeDec()
                            LastAction = "Volume -"
                        Case "+", "plus"
                            iTunesObject.volumeInc()
                            LastAction = "Volume +"
                        Case Else
                            LastAction = "Inconnu"
                    End Select
                Case Else
                    LastTypeAction = "Type d'action"
                    LastAction = "non reconnu"
            End Select

        Next
        Return ListNoms(index)
    End Function
#End Region

#Region "Méthodes"
    Private Sub CapturerGraphique()
        If bCapturer.Text = "Arrêter la capture" Then
            WaitingCapture = False
            bCapturer.Text = "Capturer"
            Dim NewTriggerValue As String = ""
            For Each Trigger In ListBoxTriggers.Items
                NewTriggerValue &= Trigger & ","
            Next
            NewTriggerValue = NewTriggerValue.Substring(0, NewTriggerValue.Length - 1)
            ListSignaux(ListID(ListBoxDescription.SelectedIndex)) = NewTriggerValue
            WriteFileFromLists()
        Else
            If Me.Connected = False Then
                ManuellementToolStripMenuItem_Click(Nothing, Nothing)
                Exit Sub
            End If
            WaitingCapture = True
            If MainParametersProvider.GraphicMode Then
                bCapturer.Text = "Arrêter la capture"
            End If
        End If
    End Sub
    Public Sub DestroyDependancies()
        WizmoInstance.Delete()
        NirCMDInstance.Delete()
    End Sub
    Private Sub PopulateLists()
        If (tConfig.Text) = "" Then Exit Sub
        ListBoxDescription.Items.Clear()
        ListBoxTriggers.Items.Clear()
        ListBoxActions.Items.Clear()
        For Each ID In ListID
            ListBoxDescription.Items.Add(ListNoms(ID))
        Next
        ListBoxDescription.SelectedIndex = 0
    End Sub

    Private Sub PopulateTriggersAndActions()
        If (ListBoxDescription.SelectedIndex = -1) Then Exit Sub
        ListBoxTriggers.Items.Clear()
        ListBoxActions.Items.Clear()
        Try
            For Each Trigger In ListSignaux(ListID(ListBoxDescription.SelectedIndex)).Split(",")
                If Trigger <> "" Then ListBoxTriggers.Items.Add(Trigger)
            Next
        Catch ex As Exception

        End Try
        Try
            For Each Action In ListActions(ListID(ListBoxDescription.SelectedIndex)).Split("|")
                If Action <> "" Then ListBoxActions.Items.Add(MainConfigurationParser.Translate(Action))
            Next
        Catch ex As Exception

        End Try

    End Sub

    Public Sub UpdateLayout()
        'Général
        Me.Width = 1040
        'tConfig.Width = 462
        'tConfig.Location = New Point(520, 43)
        Me.Icon = My.Resources.Icone_V2

        'Partie connecté / déconnecté
        If Not Connected() Then
            bConnecter.Width = 207
            bDeconnecter.Width = 0
        Else
            bConnecter.Width = 0
            bDeconnecter.Width = 207
        End If
        bDeconnecter.Left = bConnecter.Left

        'Partie log activé / désactivé
        Dim DisplayLog As Boolean = MainParametersProvider.DisplayLog
        PanelConnectionControl.Location = New Point(15, 15)
        If DisplayLog Then
            Me.Height = 500
            Me.Width = 1036
            PanelDroite.Location = New Point(519, 15)
            PanelContentDroite.Location = New Point(520, 43)
            bClearLog.Location = New Point(15, 428)
            bParametres.Location = New Point(350, 428)
            bAide.Location = New Point(431, 428)
            bAide.Width = 75
            bParametres.Width = 75
            bClearLog.Visible = True
        Else
            PanelDroite.Location = New Point(15, PanelConnectionControl.Top + PanelConnectionControl.Height + 5) '5px de marge
            PanelContentDroite.Location = New Point(16, PanelConnectionControl.Top + PanelConnectionControl.Height + 5 + PanelDroite.Height + 5)
            bAide.Top = PanelConnectionControl.Top + PanelConnectionControl.Height + 5 + PanelDroite.Height + 5 + PanelContentDroite.Height + 5
            bParametres.Top = PanelConnectionControl.Top + PanelConnectionControl.Height + 5 + PanelDroite.Height + 5 + PanelContentDroite.Height + 5
            bClearLog.Visible = False
            Me.Height = 523
            Me.Width = 536
            bAide.Left = 346
            bAide.Width = 162
            bParametres.Width = 162
            bParametres.Left = 15
        End If
        tLog.Visible = DisplayLog

        'Partie mode graphique / mode manuel
        Dim GraphicMode As Boolean = MainParametersProvider.GraphicMode
        bNouveau.Visible = GraphicMode
        ListBoxActions.Visible = GraphicMode
        ListBoxDescription.Visible = GraphicMode
        ListBoxTriggers.Visible = GraphicMode
        lblTriggers.Visible = GraphicMode
        lblEvenements.Visible = GraphicMode
        lblActions.Visible = GraphicMode
        tConfig.Visible = Not GraphicMode
        lblParseError.Visible = Not GraphicMode
        bCompiler.Visible = Not GraphicMode
        bEnregistrer.Visible = Not GraphicMode
        bAjouter.Enabled = Not GraphicMode
        bCapturer.Enabled = Not GraphicMode
        If GraphicMode Then
            lblConfigModifiee.Location = New Point(517, 20)
            bCapturer.Location = New Point(165, -1)
            bNouveau.Location = New Point(0, -1)
            bAjouter.Location = New Point(330, -1)
            bCapturer.Width = ListBoxTriggers.Width + 2
            bAjouter.Width = ListBoxActions.Width + 2
            bNouveau.Width = ListBoxDescription.Width + 2
            'ListBoxDescription.Height = 394
            'ListBoxTriggers.Height = 394
            'ListBoxActions.Height = 394
            PopulateLists()
        Else
            lblConfigModifiee.Location = New Point(517, 20)
            bCapturer.Location = New Point(826, 16)
            bAjouter.Location = New Point(907, 16)
            bAjouter.Width = 75
            bCapturer.Width = 75
        End If

        'Partie connexion
        cmbPort.Visible = cmbPort.Items.Count > 0
        cmbBaud.Visible = cmbPort.Items.Count > 0
        lblBaud.Visible = cmbPort.Items.Count > 0
        lblPort.Visible = cmbPort.Items.Count > 0
        bConnecter.Visible = cmbPort.Items.Count > 0
        lblNoCOMDetected.Visible = Not cmbPort.Items.Count > 0
        If cmbPort.Items.Count > 0 Then cmbPort.SelectedIndex = 0
    End Sub

    Private Sub ChargerConfig()
        If My.Computer.FileSystem.FileExists(Environ("APPDATA") & "\PCRemote\" & "\config.cfg") = False Then
            tLog.Text += "[" & Heure() & "] " & MarqueurConfiguration & " Le fichier de configuration est introuvable." & vbCrLf
            Exit Sub
        End If
        Dim loadFile As New IO.StreamReader(Environ("APPDATA") & "\PCRemote\" & "\config.cfg")
        tConfig.Text = loadFile.ReadToEnd
        loadFile.Close()
        tLog.Text += "[" & Heure() & "] " & MarqueurConfiguration & " Fichier chargé. Analyse du code..." & vbCrLf
    End Sub
    Public Sub Capturer()
        CaptureGraphique = True
    End Sub
    Public Sub StopCapturer()
        CaptureGraphique = False
    End Sub
    Public Function Connected()
        Return Connecte
    End Function
    Sub ClearLists()
        ListID.Clear()
        ListSignaux.Clear()
        ListNoms.Clear()
        ListActions.Clear()
    End Sub

    Private Function ApplyConfig()
        ClearLists()
        tConfig.Text = MainConfigurationParser.Normalize(tConfig.Text)
        If (Not Verify()) Then Return False
        Dim Analyzed As Object() = MainConfigurationParser.Analyze(tConfig.Text)
        ListID = Analyzed(0)
        ListNoms = Analyzed(1)
        ListSignaux = Analyzed(2)
        ListActions = Analyzed(3)
        lblConfigModifiee.Visible = False
        tLog.Text += "[" & Heure() & "] " & MarqueurConfiguration & " Configuration analysée, aucune erreur détectée."
        Return True
    End Function
    Private Sub WriteFileFromLists()
        Dim FinalString As String = ""
        For Each ID In ListID
            FinalString &= ID & ";" & ListNoms(ID) & ";" & ListSignaux(ID) & ";" & ListActions(ID) & vbCrLf
        Next
        FinalString = MainConfigurationParser.Normalize(FinalString)
        Dim saveFile As New IO.StreamWriter(Environ("APPDATA") & "\PCRemote\" & "\config.cfg", False)
        Debug.Print(FinalString)
        tConfig.Text = FinalString
        saveFile.Write(FinalString)
        saveFile.Close()
        ApplyConfig()
    End Sub
#End Region
End Class
