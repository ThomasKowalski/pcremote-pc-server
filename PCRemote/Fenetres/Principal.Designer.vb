﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.bConnecter = New System.Windows.Forms.Button()
        Me.cmbPort = New System.Windows.Forms.ComboBox()
        Me.cmbBaud = New System.Windows.Forms.ComboBox()
        Me.bDeconnecter = New System.Windows.Forms.Button()
        Me.tLog = New System.Windows.Forms.TextBox()
        Me.lblPort = New System.Windows.Forms.Label()
        Me.lblBaud = New System.Windows.Forms.Label()
        Me.tConfig = New System.Windows.Forms.TextBox()
        Me.TimerReception = New System.Windows.Forms.Timer(Me.components)
        Me.bEnregistrer = New System.Windows.Forms.Button()
        Me.bClearLog = New System.Windows.Forms.Button()
        Me.bParametres = New System.Windows.Forms.Button()
        Me.TimerCoDeco = New System.Windows.Forms.Timer(Me.components)
        Me.bAide = New System.Windows.Forms.Button()
        Me.timerNotif = New System.Windows.Forms.Timer(Me.components)
        Me.timerConfirmConnect = New System.Windows.Forms.Timer(Me.components)
        Me.lblParseError = New System.Windows.Forms.Label()
        Me.bCompiler = New System.Windows.Forms.Button()
        Me.ContextMenuTriggers = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AjouterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CapturerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManuellementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupprimerToolStripMenuItemTriggers = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuActions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SupprimerToolStripMenuItemActions = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuDescriptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RenommerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupprimerToolStripMenuItemDescription = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblNoCOMDetected = New System.Windows.Forms.Label()
        Me.TimerPresence = New System.Windows.Forms.Timer(Me.components)
        Me.TimerPresenceReception = New System.Windows.Forms.Timer(Me.components)
        Me.PanelConnectionControl = New System.Windows.Forms.Panel()
        Me.PanelDroite = New System.Windows.Forms.Panel()
        Me.bNouveau = New System.Windows.Forms.Button()
        Me.bCapturer = New System.Windows.Forms.Button()
        Me.bAjouter = New System.Windows.Forms.Button()
        Me.lblConfigModifiee = New System.Windows.Forms.Label()
        Me.PanelContentDroite = New System.Windows.Forms.Panel()
        Me.lblActions = New System.Windows.Forms.Label()
        Me.lblTriggers = New System.Windows.Forms.Label()
        Me.lblEvenements = New System.Windows.Forms.Label()
        Me.ListBoxActions = New System.Windows.Forms.ListBox()
        Me.ListBoxTriggers = New System.Windows.Forms.ListBox()
        Me.ListBoxDescription = New System.Windows.Forms.ListBox()
        Me.ContextMenuTriggers.SuspendLayout()
        Me.ContextMenuActions.SuspendLayout()
        Me.ContextMenuDescriptions.SuspendLayout()
        Me.PanelConnectionControl.SuspendLayout()
        Me.PanelDroite.SuspendLayout()
        Me.PanelContentDroite.SuspendLayout()
        Me.SuspendLayout()
        '
        'bConnecter
        '
        Me.bConnecter.Location = New System.Drawing.Point(285, -1)
        Me.bConnecter.Name = "bConnecter"
        Me.bConnecter.Size = New System.Drawing.Size(101, 23)
        Me.bConnecter.TabIndex = 0
        Me.bConnecter.Text = "Connecter"
        Me.bConnecter.UseVisualStyleBackColor = True
        '
        'cmbPort
        '
        Me.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPort.FormattingEnabled = True
        Me.cmbPort.Location = New System.Drawing.Point(39, 0)
        Me.cmbPort.Name = "cmbPort"
        Me.cmbPort.Size = New System.Drawing.Size(132, 21)
        Me.cmbPort.TabIndex = 1
        '
        'cmbBaud
        '
        Me.cmbBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBaud.FormattingEnabled = True
        Me.cmbBaud.Location = New System.Drawing.Point(221, 0)
        Me.cmbBaud.Name = "cmbBaud"
        Me.cmbBaud.Size = New System.Drawing.Size(58, 21)
        Me.cmbBaud.TabIndex = 2
        '
        'bDeconnecter
        '
        Me.bDeconnecter.Location = New System.Drawing.Point(392, -1)
        Me.bDeconnecter.Name = "bDeconnecter"
        Me.bDeconnecter.Size = New System.Drawing.Size(101, 23)
        Me.bDeconnecter.TabIndex = 3
        Me.bDeconnecter.Text = "Déconnecter"
        Me.bDeconnecter.UseVisualStyleBackColor = True
        '
        'tLog
        '
        Me.tLog.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tLog.Location = New System.Drawing.Point(16, 43)
        Me.tLog.Multiline = True
        Me.tLog.Name = "tLog"
        Me.tLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.tLog.Size = New System.Drawing.Size(489, 380)
        Me.tLog.TabIndex = 4
        '
        'lblPort
        '
        Me.lblPort.AutoSize = True
        Me.lblPort.Location = New System.Drawing.Point(1, 3)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(32, 13)
        Me.lblPort.TabIndex = 5
        Me.lblPort.Text = "Port :"
        '
        'lblBaud
        '
        Me.lblBaud.AutoSize = True
        Me.lblBaud.Location = New System.Drawing.Point(177, 3)
        Me.lblBaud.Name = "lblBaud"
        Me.lblBaud.Size = New System.Drawing.Size(38, 13)
        Me.lblBaud.TabIndex = 6
        Me.lblBaud.Text = "Baud :"
        '
        'tConfig
        '
        Me.tConfig.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tConfig.Location = New System.Drawing.Point(0, 0)
        Me.tConfig.Multiline = True
        Me.tConfig.Name = "tConfig"
        Me.tConfig.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.tConfig.Size = New System.Drawing.Size(491, 380)
        Me.tConfig.TabIndex = 4
        '
        'TimerReception
        '
        '
        'bEnregistrer
        '
        Me.bEnregistrer.Location = New System.Drawing.Point(417, -1)
        Me.bEnregistrer.Name = "bEnregistrer"
        Me.bEnregistrer.Size = New System.Drawing.Size(75, 23)
        Me.bEnregistrer.TabIndex = 9
        Me.bEnregistrer.Text = "Enregistrer"
        Me.bEnregistrer.UseVisualStyleBackColor = True
        '
        'bClearLog
        '
        Me.bClearLog.Location = New System.Drawing.Point(15, 428)
        Me.bClearLog.Name = "bClearLog"
        Me.bClearLog.Size = New System.Drawing.Size(125, 23)
        Me.bClearLog.TabIndex = 12
        Me.bClearLog.Text = "Effacer log"
        Me.bClearLog.UseVisualStyleBackColor = True
        '
        'bParametres
        '
        Me.bParametres.Location = New System.Drawing.Point(350, 428)
        Me.bParametres.Name = "bParametres"
        Me.bParametres.Size = New System.Drawing.Size(75, 23)
        Me.bParametres.TabIndex = 13
        Me.bParametres.Text = "Paramètres"
        Me.bParametres.UseVisualStyleBackColor = True
        '
        'TimerCoDeco
        '
        Me.TimerCoDeco.Interval = 250
        '
        'bAide
        '
        Me.bAide.Location = New System.Drawing.Point(431, 428)
        Me.bAide.Name = "bAide"
        Me.bAide.Size = New System.Drawing.Size(75, 23)
        Me.bAide.TabIndex = 14
        Me.bAide.Text = "Aide"
        Me.bAide.UseVisualStyleBackColor = True
        '
        'timerNotif
        '
        Me.timerNotif.Interval = 1
        '
        'timerConfirmConnect
        '
        Me.timerConfirmConnect.Interval = 5000
        '
        'lblParseError
        '
        Me.lblParseError.AutoSize = True
        Me.lblParseError.Location = New System.Drawing.Point(3, 3)
        Me.lblParseError.Name = "lblParseError"
        Me.lblParseError.Size = New System.Drawing.Size(221, 13)
        Me.lblParseError.TabIndex = 15
        Me.lblParseError.Text = "Cliquez sur Vérifier pour commencer l'analyse."
        '
        'bCompiler
        '
        Me.bCompiler.Location = New System.Drawing.Point(336, -1)
        Me.bCompiler.Name = "bCompiler"
        Me.bCompiler.Size = New System.Drawing.Size(75, 23)
        Me.bCompiler.TabIndex = 9
        Me.bCompiler.Text = "Vérifier"
        Me.bCompiler.UseVisualStyleBackColor = True
        '
        'ContextMenuTriggers
        '
        Me.ContextMenuTriggers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AjouterToolStripMenuItem, Me.SupprimerToolStripMenuItemTriggers})
        Me.ContextMenuTriggers.Name = "ContextMenuActions"
        Me.ContextMenuTriggers.Size = New System.Drawing.Size(130, 48)
        '
        'AjouterToolStripMenuItem
        '
        Me.AjouterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CapturerToolStripMenuItem, Me.ManuellementToolStripMenuItem})
        Me.AjouterToolStripMenuItem.Name = "AjouterToolStripMenuItem"
        Me.AjouterToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.AjouterToolStripMenuItem.Text = "Ajouter"
        '
        'CapturerToolStripMenuItem
        '
        Me.CapturerToolStripMenuItem.Name = "CapturerToolStripMenuItem"
        Me.CapturerToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.CapturerToolStripMenuItem.Text = "Capturer"
        '
        'ManuellementToolStripMenuItem
        '
        Me.ManuellementToolStripMenuItem.Name = "ManuellementToolStripMenuItem"
        Me.ManuellementToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ManuellementToolStripMenuItem.Text = "Manuellement"
        '
        'SupprimerToolStripMenuItemTriggers
        '
        Me.SupprimerToolStripMenuItemTriggers.Name = "SupprimerToolStripMenuItemTriggers"
        Me.SupprimerToolStripMenuItemTriggers.Size = New System.Drawing.Size(129, 22)
        Me.SupprimerToolStripMenuItemTriggers.Text = "Supprimer"
        '
        'ContextMenuActions
        '
        Me.ContextMenuActions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SupprimerToolStripMenuItemActions})
        Me.ContextMenuActions.Name = "ContextMenuActions"
        Me.ContextMenuActions.Size = New System.Drawing.Size(130, 26)
        '
        'SupprimerToolStripMenuItemActions
        '
        Me.SupprimerToolStripMenuItemActions.Name = "SupprimerToolStripMenuItemActions"
        Me.SupprimerToolStripMenuItemActions.Size = New System.Drawing.Size(129, 22)
        Me.SupprimerToolStripMenuItemActions.Text = "Supprimer"
        '
        'ContextMenuDescriptions
        '
        Me.ContextMenuDescriptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RenommerToolStripMenuItem, Me.SupprimerToolStripMenuItemDescription})
        Me.ContextMenuDescriptions.Name = "ContextMenuActions"
        Me.ContextMenuDescriptions.Size = New System.Drawing.Size(134, 48)
        '
        'RenommerToolStripMenuItem
        '
        Me.RenommerToolStripMenuItem.Name = "RenommerToolStripMenuItem"
        Me.RenommerToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.RenommerToolStripMenuItem.Text = "Renommer"
        '
        'SupprimerToolStripMenuItemDescription
        '
        Me.SupprimerToolStripMenuItemDescription.Name = "SupprimerToolStripMenuItemDescription"
        Me.SupprimerToolStripMenuItemDescription.Size = New System.Drawing.Size(133, 22)
        Me.SupprimerToolStripMenuItemDescription.Text = "Supprimer"
        '
        'lblNoCOMDetected
        '
        Me.lblNoCOMDetected.AutoSize = True
        Me.lblNoCOMDetected.Location = New System.Drawing.Point(1, 3)
        Me.lblNoCOMDetected.Name = "lblNoCOMDetected"
        Me.lblNoCOMDetected.Size = New System.Drawing.Size(332, 13)
        Me.lblNoCOMDetected.TabIndex = 5
        Me.lblNoCOMDetected.Text = "Aucun matériel connecté en série détecté. Cliquez ici pour actualiser."
        Me.lblNoCOMDetected.Visible = False
        '
        'TimerPresence
        '
        Me.TimerPresence.Interval = 3000
        '
        'TimerPresenceReception
        '
        Me.TimerPresenceReception.Interval = 2000
        '
        'PanelConnectionControl
        '
        Me.PanelConnectionControl.Controls.Add(Me.bConnecter)
        Me.PanelConnectionControl.Controls.Add(Me.bDeconnecter)
        Me.PanelConnectionControl.Controls.Add(Me.lblPort)
        Me.PanelConnectionControl.Controls.Add(Me.cmbPort)
        Me.PanelConnectionControl.Controls.Add(Me.cmbBaud)
        Me.PanelConnectionControl.Controls.Add(Me.lblBaud)
        Me.PanelConnectionControl.Controls.Add(Me.lblNoCOMDetected)
        Me.PanelConnectionControl.Location = New System.Drawing.Point(14, 15)
        Me.PanelConnectionControl.Name = "PanelConnectionControl"
        Me.PanelConnectionControl.Size = New System.Drawing.Size(492, 21)
        Me.PanelConnectionControl.TabIndex = 24
        '
        'PanelDroite
        '
        Me.PanelDroite.Controls.Add(Me.bNouveau)
        Me.PanelDroite.Controls.Add(Me.bCapturer)
        Me.PanelDroite.Controls.Add(Me.bAjouter)
        Me.PanelDroite.Controls.Add(Me.lblConfigModifiee)
        Me.PanelDroite.Controls.Add(Me.bEnregistrer)
        Me.PanelDroite.Controls.Add(Me.bCompiler)
        Me.PanelDroite.Controls.Add(Me.lblParseError)
        Me.PanelDroite.Location = New System.Drawing.Point(519, 15)
        Me.PanelDroite.Name = "PanelDroite"
        Me.PanelDroite.Size = New System.Drawing.Size(492, 21)
        Me.PanelDroite.TabIndex = 25
        '
        'bNouveau
        '
        Me.bNouveau.Location = New System.Drawing.Point(-1, -1)
        Me.bNouveau.Name = "bNouveau"
        Me.bNouveau.Size = New System.Drawing.Size(87, 23)
        Me.bNouveau.TabIndex = 25
        Me.bNouveau.Text = "Nouveau"
        Me.bNouveau.UseVisualStyleBackColor = True
        '
        'bCapturer
        '
        Me.bCapturer.Location = New System.Drawing.Point(165, -1)
        Me.bCapturer.Name = "bCapturer"
        Me.bCapturer.Size = New System.Drawing.Size(91, 23)
        Me.bCapturer.TabIndex = 22
        Me.bCapturer.Text = "Capturer"
        Me.bCapturer.UseVisualStyleBackColor = True
        '
        'bAjouter
        '
        Me.bAjouter.Location = New System.Drawing.Point(330, -1)
        Me.bAjouter.Name = "bAjouter"
        Me.bAjouter.Size = New System.Drawing.Size(77, 23)
        Me.bAjouter.TabIndex = 24
        Me.bAjouter.Text = "Ajouter"
        Me.bAjouter.UseVisualStyleBackColor = True
        '
        'lblConfigModifiee
        '
        Me.lblConfigModifiee.AutoSize = True
        Me.lblConfigModifiee.Location = New System.Drawing.Point(0, 4)
        Me.lblConfigModifiee.Name = "lblConfigModifiee"
        Me.lblConfigModifiee.Size = New System.Drawing.Size(238, 13)
        Me.lblConfigModifiee.TabIndex = 23
        Me.lblConfigModifiee.Text = "Configuration modifiée manuellement [Enregistrer]"
        Me.lblConfigModifiee.Visible = False
        '
        'PanelContentDroite
        '
        Me.PanelContentDroite.Controls.Add(Me.lblActions)
        Me.PanelContentDroite.Controls.Add(Me.lblTriggers)
        Me.PanelContentDroite.Controls.Add(Me.lblEvenements)
        Me.PanelContentDroite.Controls.Add(Me.ListBoxActions)
        Me.PanelContentDroite.Controls.Add(Me.ListBoxTriggers)
        Me.PanelContentDroite.Controls.Add(Me.ListBoxDescription)
        Me.PanelContentDroite.Controls.Add(Me.tConfig)
        Me.PanelContentDroite.Location = New System.Drawing.Point(520, 43)
        Me.PanelContentDroite.Name = "PanelContentDroite"
        Me.PanelContentDroite.Size = New System.Drawing.Size(491, 382)
        Me.PanelContentDroite.TabIndex = 26
        '
        'lblActions
        '
        Me.lblActions.AutoSize = True
        Me.lblActions.Location = New System.Drawing.Point(328, -1)
        Me.lblActions.Name = "lblActions"
        Me.lblActions.Size = New System.Drawing.Size(48, 13)
        Me.lblActions.TabIndex = 27
        Me.lblActions.Text = "Actions :"
        '
        'lblTriggers
        '
        Me.lblTriggers.AutoSize = True
        Me.lblTriggers.Location = New System.Drawing.Point(163, -1)
        Me.lblTriggers.Name = "lblTriggers"
        Me.lblTriggers.Size = New System.Drawing.Size(51, 13)
        Me.lblTriggers.TabIndex = 28
        Me.lblTriggers.Text = "Signaux :"
        '
        'lblEvenements
        '
        Me.lblEvenements.AutoSize = True
        Me.lblEvenements.Location = New System.Drawing.Point(-4, -1)
        Me.lblEvenements.Name = "lblEvenements"
        Me.lblEvenements.Size = New System.Drawing.Size(72, 13)
        Me.lblEvenements.TabIndex = 29
        Me.lblEvenements.Text = "Evénements :"
        '
        'ListBoxActions
        '
        Me.ListBoxActions.ContextMenuStrip = Me.ContextMenuActions
        Me.ListBoxActions.FormattingEnabled = True
        Me.ListBoxActions.Location = New System.Drawing.Point(331, 13)
        Me.ListBoxActions.Name = "ListBoxActions"
        Me.ListBoxActions.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.ListBoxActions.Size = New System.Drawing.Size(160, 368)
        Me.ListBoxActions.TabIndex = 25
        '
        'ListBoxTriggers
        '
        Me.ListBoxTriggers.ContextMenuStrip = Me.ContextMenuTriggers
        Me.ListBoxTriggers.FormattingEnabled = True
        Me.ListBoxTriggers.Location = New System.Drawing.Point(166, 13)
        Me.ListBoxTriggers.Name = "ListBoxTriggers"
        Me.ListBoxTriggers.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.ListBoxTriggers.Size = New System.Drawing.Size(160, 368)
        Me.ListBoxTriggers.TabIndex = 24
        '
        'ListBoxDescription
        '
        Me.ListBoxDescription.ContextMenuStrip = Me.ContextMenuDescriptions
        Me.ListBoxDescription.FormattingEnabled = True
        Me.ListBoxDescription.Location = New System.Drawing.Point(0, 13)
        Me.ListBoxDescription.Name = "ListBoxDescription"
        Me.ListBoxDescription.Size = New System.Drawing.Size(160, 368)
        Me.ListBoxDescription.TabIndex = 26
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1217, 506)
        Me.Controls.Add(Me.PanelContentDroite)
        Me.Controls.Add(Me.PanelDroite)
        Me.Controls.Add(Me.PanelConnectionControl)
        Me.Controls.Add(Me.bAide)
        Me.Controls.Add(Me.bParametres)
        Me.Controls.Add(Me.bClearLog)
        Me.Controls.Add(Me.tLog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Principal"
        Me.Text = "PC Remote"
        Me.ContextMenuTriggers.ResumeLayout(false)
        Me.ContextMenuActions.ResumeLayout(false)
        Me.ContextMenuDescriptions.ResumeLayout(false)
        Me.PanelConnectionControl.ResumeLayout(false)
        Me.PanelConnectionControl.PerformLayout
        Me.PanelDroite.ResumeLayout(false)
        Me.PanelDroite.PerformLayout
        Me.PanelContentDroite.ResumeLayout(false)
        Me.PanelContentDroite.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents bConnecter As System.Windows.Forms.Button
    Friend WithEvents cmbPort As System.Windows.Forms.ComboBox
    Friend WithEvents cmbBaud As System.Windows.Forms.ComboBox
    Friend WithEvents bDeconnecter As System.Windows.Forms.Button
    Friend WithEvents tLog As System.Windows.Forms.TextBox
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents lblBaud As System.Windows.Forms.Label
    Friend WithEvents tConfig As System.Windows.Forms.TextBox
    Friend WithEvents TimerReception As System.Windows.Forms.Timer
    Friend WithEvents bEnregistrer As System.Windows.Forms.Button
    Friend WithEvents bClearLog As System.Windows.Forms.Button
    Friend WithEvents bParametres As System.Windows.Forms.Button
    Friend WithEvents TimerCoDeco As System.Windows.Forms.Timer
    Friend WithEvents bAide As System.Windows.Forms.Button
    Friend WithEvents timerNotif As System.Windows.Forms.Timer
    Friend WithEvents timerConfirmConnect As System.Windows.Forms.Timer
    Friend WithEvents lblParseError As System.Windows.Forms.Label
    Friend WithEvents bCompiler As System.Windows.Forms.Button
    Friend WithEvents ContextMenuActions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SupprimerToolStripMenuItemActions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuDescriptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SupprimerToolStripMenuItemDescription As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblNoCOMDetected As System.Windows.Forms.Label
    Friend WithEvents ContextMenuTriggers As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SupprimerToolStripMenuItemTriggers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TimerPresence As System.Windows.Forms.Timer
    Friend WithEvents TimerPresenceReception As System.Windows.Forms.Timer
    Friend WithEvents PanelConnectionControl As System.Windows.Forms.Panel
    Friend WithEvents PanelDroite As System.Windows.Forms.Panel
    Friend WithEvents bNouveau As System.Windows.Forms.Button
    Friend WithEvents bAjouter As System.Windows.Forms.Button
    Friend WithEvents bCapturer As System.Windows.Forms.Button
    Friend WithEvents lblConfigModifiee As System.Windows.Forms.Label
    Friend WithEvents PanelContentDroite As System.Windows.Forms.Panel
    Friend WithEvents lblActions As System.Windows.Forms.Label
    Friend WithEvents lblTriggers As System.Windows.Forms.Label
    Friend WithEvents lblEvenements As System.Windows.Forms.Label
    Friend WithEvents ListBoxActions As System.Windows.Forms.ListBox
    Friend WithEvents ListBoxTriggers As System.Windows.Forms.ListBox
    Friend WithEvents ListBoxDescription As System.Windows.Forms.ListBox
    Friend WithEvents RenommerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjouterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CapturerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManuellementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
