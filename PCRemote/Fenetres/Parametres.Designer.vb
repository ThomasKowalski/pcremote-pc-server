﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Parametres
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Parametres))
        Me.cbDemarrage = New System.Windows.Forms.CheckBox()
        Me.tbFolderScreenshots = New System.Windows.Forms.TextBox()
        Me.ChoisirDossier = New System.Windows.Forms.FolderBrowserDialog()
        Me.lblDossierCaptures = New System.Windows.Forms.Label()
        Me.bBrowseFolderScreenshots = New System.Windows.Forms.Button()
        Me.bSave = New System.Windows.Forms.Button()
        Me.tbNomOrdinateur = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.toolTipAide = New System.Windows.Forms.ToolTip(Me.components)
        Me.cbModeGraphique = New System.Windows.Forms.CheckBox()
        Me.cbDisplayLog = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbDemarrage
        '
        Me.cbDemarrage.AutoSize = True
        Me.cbDemarrage.Location = New System.Drawing.Point(12, 98)
        Me.cbDemarrage.Name = "cbDemarrage"
        Me.cbDemarrage.Size = New System.Drawing.Size(127, 17)
        Me.cbDemarrage.TabIndex = 0
        Me.cbDemarrage.Text = "Lancer au démarrage"
        Me.cbDemarrage.UseVisualStyleBackColor = True
        '
        'tbFolderScreenshots
        '
        Me.tbFolderScreenshots.Location = New System.Drawing.Point(12, 26)
        Me.tbFolderScreenshots.Name = "tbFolderScreenshots"
        Me.tbFolderScreenshots.Size = New System.Drawing.Size(260, 20)
        Me.tbFolderScreenshots.TabIndex = 1
        '
        'lblDossierCaptures
        '
        Me.lblDossierCaptures.AutoSize = True
        Me.lblDossierCaptures.Location = New System.Drawing.Point(9, 10)
        Me.lblDossierCaptures.Name = "lblDossierCaptures"
        Me.lblDossierCaptures.Size = New System.Drawing.Size(150, 13)
        Me.lblDossierCaptures.TabIndex = 2
        Me.lblDossierCaptures.Text = "Dossier des captures d'écran :"
        '
        'bBrowseFolderScreenshots
        '
        Me.bBrowseFolderScreenshots.Location = New System.Drawing.Point(281, 24)
        Me.bBrowseFolderScreenshots.Name = "bBrowseFolderScreenshots"
        Me.bBrowseFolderScreenshots.Size = New System.Drawing.Size(75, 23)
        Me.bBrowseFolderScreenshots.TabIndex = 3
        Me.bBrowseFolderScreenshots.Text = "Parcourir"
        Me.bBrowseFolderScreenshots.UseVisualStyleBackColor = True
        '
        'bSave
        '
        Me.bSave.Location = New System.Drawing.Point(239, 132)
        Me.bSave.Name = "bSave"
        Me.bSave.Size = New System.Drawing.Size(117, 23)
        Me.bSave.TabIndex = 4
        Me.bSave.Text = "Enregistrer"
        Me.bSave.UseVisualStyleBackColor = True
        '
        'tbNomOrdinateur
        '
        Me.tbNomOrdinateur.Location = New System.Drawing.Point(12, 69)
        Me.tbNomOrdinateur.Name = "tbNomOrdinateur"
        Me.tbNomOrdinateur.Size = New System.Drawing.Size(344, 20)
        Me.tbNomOrdinateur.TabIndex = 1
        Me.toolTipAide.SetToolTip(Me.tbNomOrdinateur, "L'écran a une largeur de seize caractères. Votre nom ne peut pas dépasser cette l" & _
        "ongueur.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Nom de l'ordinateur :"
        '
        'cbModeGraphique
        '
        Me.cbModeGraphique.AutoSize = True
        Me.cbModeGraphique.Location = New System.Drawing.Point(12, 118)
        Me.cbModeGraphique.Name = "cbModeGraphique"
        Me.cbModeGraphique.Size = New System.Drawing.Size(103, 17)
        Me.cbModeGraphique.TabIndex = 5
        Me.cbModeGraphique.Text = "Mode graphique"
        Me.cbModeGraphique.UseVisualStyleBackColor = True
        '
        'cbDisplayLog
        '
        Me.cbDisplayLog.AutoSize = True
        Me.cbDisplayLog.Location = New System.Drawing.Point(12, 138)
        Me.cbDisplayLog.Name = "cbDisplayLog"
        Me.cbDisplayLog.Size = New System.Drawing.Size(90, 17)
        Me.cbDisplayLog.TabIndex = 6
        Me.cbDisplayLog.Text = "Afficher le log"
        Me.cbDisplayLog.UseVisualStyleBackColor = True
        '
        'Parametres
        '
        Me.AcceptButton = Me.bSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 167)
        Me.Controls.Add(Me.cbDisplayLog)
        Me.Controls.Add(Me.cbModeGraphique)
        Me.Controls.Add(Me.bSave)
        Me.Controls.Add(Me.bBrowseFolderScreenshots)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDossierCaptures)
        Me.Controls.Add(Me.tbNomOrdinateur)
        Me.Controls.Add(Me.tbFolderScreenshots)
        Me.Controls.Add(Me.cbDemarrage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Parametres"
        Me.Text = "Paramètres"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbDemarrage As System.Windows.Forms.CheckBox
    Friend WithEvents tbFolderScreenshots As System.Windows.Forms.TextBox
    Friend WithEvents ChoisirDossier As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lblDossierCaptures As System.Windows.Forms.Label
    Friend WithEvents bBrowseFolderScreenshots As System.Windows.Forms.Button
    Friend WithEvents bSave As System.Windows.Forms.Button
    Friend WithEvents tbNomOrdinateur As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents toolTipAide As System.Windows.Forms.ToolTip
    Friend WithEvents cbModeGraphique As System.Windows.Forms.CheckBox
    Friend WithEvents cbDisplayLog As System.Windows.Forms.CheckBox
End Class
