﻿Public Class PanelVisualisation
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer

    Public Sub ModifierInfos(connecte As Boolean, port As String, baud As Integer, lastAction As String)
        If connecte Then 'Modification de l'UI en fonction des infos de l'application : connecté ou pas, baud, port, dernière action...
            Me.Height = 135
            IconeNotification.BalloonTipIcon = ToolTipIcon.Info
            IconeNotification.BalloonTipText = "PCRemote est correctement connecté à l'Arduino !"
            IconeNotification.BalloonTipTitle = "Connecté !"
            lblPort.Text = port
            lblLastAction.Text = lastAction
            lblConnecte.Text = "Connecté !"
            lblConnecte.ForeColor = Color.Green
        Else
            Me.Height = 65
            IconeNotification.BalloonTipIcon = ToolTipIcon.Error
            IconeNotification.BalloonTipText = "PCRemote n'est pas connecté."
            IconeNotification.BalloonTipTitle = "Déconnecté."
            lblConnecte.Text = "Déconnecté."
            lblConnecte.ForeColor = Color.Red
        End If
    End Sub

    Private Sub PanelVisualisation_DoubleClick(sender As Object, e As EventArgs) Handles Me.DoubleClick, lblConnecte.DoubleClick, lblTitre.DoubleClick, lblPort.DoubleClick, lblLastAction.DoubleClick 'Permet de basculer entre fenêtre au premier plan / fenêtre normale
        MainParametersProvider.TopMost = Not MainParametersProvider.TopMost
        PremierPlanToolStripMenuItem.Checked = MainParametersProvider.TopMost
        Me.TopMost = MainParametersProvider.TopMost
        MainParametersProvider.ChangeParameter("TopMost", MainParametersProvider.TopMost)
    End Sub

    Private Sub PanelVisualisation_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        IconeNotification.Visible = False 'Cache la notification lors de la fermeture de l'application... Enfin c'est ce que c'est censé faire mais dans la pratique ça marche pas.
    End Sub

    Private Sub PanelVisualisation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.Icone_V2
        IconeNotification.Visible = True
        IconeNotification.Icon = My.Resources.Icone_V2
        Me.TopMost = MainParametersProvider.TopMost
        Me.Location = New Point(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "PanelX", Me.Location.X), My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "PanelY", Me.Location.Y)) 'On met la position que l'on charge du registre
    End Sub

    Private Sub IconeNotification_MouseClick(sender As Object, e As MouseEventArgs) Handles IconeNotification.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            MenuIcone.Show()
        Else
            Principal.Show()
        End If
    End Sub

    Private Sub ParamètresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ParamètresToolStripMenuItem.Click
        Principal.Show()
    End Sub

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        IconeNotification.Visible = False
        If Principal.sp.IsOpen() Then
            If Principal.tryAutoDisconnect() Then 'On essaie de fermer automatiquement la connexion (sans intervention de l'utilisateur)
                'Bah on fait rien si on arrive à fermer automatiquement...
            Else
                Principal.tLog.Text += "[" & Heure() & "] " & MarqueurConnexion & " Veuillez déconnecter avant de fermer." & vbCrLf 'Et si on y arrive pas on lui dit
                Exit Sub
            End If
        End If
        Try
            Principal.tLog.Text += "[" & Heure() & "] " & MarqueurDependances & " Suppression des dépendances temporaires..." & vbCrLf
            Principal.DestroyDependancies()
            Principal.tLog.Text += "[" & Heure() & "] " & MarqueurDependances & " Dépendances temporaires supprimées." & vbCrLf
        Catch ex As Exception
            Principal.tLog.Text += "[" & Heure() & "] " & MarqueurDependances & " Impossible de supprimer les dépendances temporaires." & vbCrLf
            Exit Sub
        End Try
        Principal.tLog.Text += "[" & Heure() & "] " & MarqueurCore & " PCRemote stopped with code 0x00." & vbCrLf
        MainLogProvider.WriteLog(Principal.tLog.Text)
        End
    End Sub

    Private Sub PanneauDaffichageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PanneauDaffichageToolStripMenuItem.Click
        If PanneauDaffichageToolStripMenuItem.Checked Then
            Me.Show()
        Else
            Me.Hide()
        End If
    End Sub

#Region "Position"
    'Les trois subs d'après concernent la gestion de la position de la fenêtre
    Private Sub PanelVisualisation_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown, lblConnecte.MouseDown, lblTitre.MouseDown, lblPort.MouseDown, lblLastAction.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top

    End Sub

    Private Sub PanelVisualisation_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove, lblConnecte.MouseMove, lblTitre.MouseMove, lblPort.MouseMove, lblLastAction.MouseMove
        If drag Then
            Me.Top = Windows.Forms.Cursor.Position.Y - mousey
            Me.Left = Windows.Forms.Cursor.Position.X - mousex
        End If
    End Sub

    Private Sub PanelVisualisation_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp, lblConnecte.MouseUp, lblTitre.MouseUp, lblPort.MouseUp, lblLastAction.MouseUp
        drag = False
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "PanelX", Me.Location.X)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Thomas Kowalski\PC Remote", "PanelY", Me.Location.Y)
    End Sub
#End Region

    Private Sub PremierPlanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PremierPlanToolStripMenuItem.Click
        MainParametersProvider.ChangeParameter("TopMost", Not MainParametersProvider.TopMost)
        Me.TopMost = MainParametersProvider.TopMost
        PremierPlanToolStripMenuItem.Checked = MainParametersProvider.TopMost
        MainParametersProvider.TopMost = MainParametersProvider.TopMost
    End Sub
End Class