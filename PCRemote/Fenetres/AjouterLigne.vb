﻿Public Class AjouterLigne
    Public Sub captureButton()
        If Principal.Connected Then
            bCapturer.Enabled = True
        Else
            bCapturer.Enabled = False
        End If
    End Sub

    Private Sub AjouterLigne_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Principal.StopCapturer()
    End Sub

    Private Sub bSupprimer_Click(sender As Object, e As EventArgs) Handles bSupprimer.Click
        If listTouches.SelectedItems.Count = 0 Then Exit Sub
        listTouches.Items.RemoveAt(listTouches.SelectedIndex)
    End Sub

    Private Sub bCapturer_Click(sender As Object, e As EventArgs) Handles bCapturer.Click
        If bCapturer.Text = "Capturer" Then
            Principal.Capturer()
            bCapturer.Text = "Arrêter capture"
        Else
            Principal.StopCapturer()
            bCapturer.Text = "Capturer"
        End If
    End Sub

    Private Sub bValider_Click(sender As Object, e As EventArgs) Handles bValider.Click
        Dim AddToFile As String = ""
        For i = 0 To listTouches.Items.Count - 1
            If i < listTouches.Items.Count - 1 Then
                AddToFile &= listTouches.Items(i) & ","
            Else
                AddToFile &= listTouches.Items(i) & ";"
            End If
        Next
        'Select Case treeActions.SelectedNode.Parent.Name
        'End Select
        'Select Case treeActions.SelectedNode.Parent.ToString.Substring(10, treeActions.SelectedNode.Parent.ToString.Length - 10)
        '    Case "Lancer un programme"
        '        AddToFile &= "launch"
        '    Case "iTunes"
        '        AddToFile &= "itunes"
        'End Select
        If treeActions.SelectedNode.Name = "kill" Then
            If personnalise.Text = "" Then
                MessageBox.Show("Merci d'entrer un nom de processus à terminer.", "Champ non rempli", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            AddToFile &= "kill;" & personnalise.Text
            If Principal.tConfig.Text = "" Then
                Principal.tConfig.Text &= AddToFile
            Else
                Principal.tConfig.Text &= vbCrLf & AddToFile
            End If
            Me.Close()
            Exit Sub
        End If
        AddToFile &= treeActions.SelectedNode.Parent.Name
        AddToFile &= ";"
        Dim NodeSelectionne As String = treeActions.SelectedNode.ToString.Substring(10, treeActions.SelectedNode.ToString.Length - 10)
        If treeActions.SelectedNode.Name = "custom" Or treeActions.SelectedNode.Name = "kill" Then
            If personnalise.Text = "" Then
                MessageBox.Show("Merci de sélectionner un fichier ou un programme à l'aide du bouton ci-dessous.", "Sélection invalide", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            AddToFile &= """" & personnalise.Text & """"
        Else
            AddToFile &= treeActions.SelectedNode.Name
        End If
        If Principal.tConfig.Text = "" Then
            Principal.tConfig.Text &= AddToFile
        Else
            Principal.tConfig.Text &= vbCrLf & AddToFile
        End If
        Me.Close()
    End Sub

    Private Sub AjouterLigne_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.Icone_V2
        If Principal.Connected Then
            bCapturer.Enabled = True
            ToolTipInfo.Active = False
        Else
            bCapturer.Enabled = False
            ToolTipInfo.Active = True
        End If
    End Sub

    Private Sub treeActions_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles treeActions.AfterSelect
        boutonValider()
    End Sub

    Private Sub personnalise_TextChanged(sender As Object, e As EventArgs) Handles personnalise.TextChanged
        boutonValider()
    End Sub

    Sub boutonValider()
        If treeActions.SelectedNode.Name = "itunes" Or treeActions.SelectedNode.Name = "vlc" Or treeActions.SelectedNode.Name = "launch" Or treeActions.SelectedNode.Name = "power" Or treeActions.SelectedNode.Name = "sound" Or treeActions.SelectedNode.Name = "others" Then
            bValider.Enabled = False
        ElseIf listTouches.Items.Count = 0 Then
            bValider.Enabled = False
        ElseIf treeActions.SelectedNode.Index = -1 Then
            bValider.Enabled = False
        ElseIf (treeActions.SelectedNode.Name = "custom" Or treeActions.SelectedNode.Name = "kill") And personnalise.Text = "" Then
            bValider.Enabled = False
        Else
            bValider.Enabled = True
        End If
        If treeActions.SelectedNode.Name = "custom" Or treeActions.SelectedNode.Name = "kill" Then
            personnalise.Enabled = True
            bParcourir.Enabled = True
        Else
            personnalise.Enabled = False
            bParcourir.Enabled = False
        End If
    End Sub

    Private Sub bParcourir_Click(sender As Object, e As EventArgs) Handles bParcourir.Click
        With SelectionnerCustom
            .AddExtension = True
            .CheckFileExists = True
            .CheckPathExists = True
            .DefaultExt = ".exe"
            .FileName = ""
            .Multiselect = False
            .RestoreDirectory = True
            .Title = "Sélectionnez le fichier personnalisé."
            .ShowDialog()
        End With
        If SelectionnerCustom.FileName <> "" Then
            personnalise.Text = SelectionnerCustom.FileName
        End If
    End Sub

    Private Sub listTouches_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listTouches.SelectedIndexChanged

    End Sub
End Class